# DTM scripts
In this folder are some scripts to perform methodological and technological developments at GeT-PlaGe.  
For the moment, the main.nf script is used to perform basic analyses. Then, specific analyses on coverage can be done using the `make_bedgraph.sh` and `circlize_v2.R` scripts.

## Coverage comparison
Comparative coverage analysis by sample can be performed using `make_bedgraph.sh` and `circlize_v2.R`.  
### 1/ Using **make_bedgraph.sh**
This bash script does:
1. renamming chromosomes/scaffolds using chrom.names file
2. BAM indexing
3. bedgraph file generating
4. removing of unwanted chromosomes/scaffolds

This script takes three mandatory non-flagged inputs:
- path to BAM files folder
- path to chrom_names file
- pattern of unwanted chromosomes/scafolds (can be a void string)

Example of command line to generate chrom_names file:
```bash 
grep "^>" genome.fa | cut -d' ' -f1,8  | sed 's/>//' - | sed 's/,//' - | tr -s ' ' '\t' > chrom_names
```
*NB: fields to keep in the cut command must be adapted for each genome.fa file.  
The second column of the chrom_names file can be written by hand if needed.*
Example of chrom_names file content:
```bash 
GK000076.1      1
GK000077.1      2
GK000078.1      3
GK000079.1      4
GK000080.1      5
GK000081.1      6
GK000082.1      7
GK000083.1      8
GK000084.1      9
GK000085.1     10
```

Example of sbatch command of `make_bedgraph.sh`:
```bash
sbatch -J bedgraph --array=1-6 make_bedgraph.sh ../samtools ../chrom_names "JANXI\|CM"
```
*NB : `make_bedgraph.sh` is an array slurm script, it runs one time per BAM file. So, the `--array=` argument must contain the number of BAM files to analyze.*

### 2/ Using **circlize_v2.R**
This R script make one circos plot for every input data.  
It takes two mandatory non-flagged arguments :
- chunk_size 
- list of bedgraph files (each file must be coma-space separated: `, `)

*NB : use `ls -m *.bedgraph` to generate the well structured list of bedgraph files*
Example of sbatch command of `circlize_v2.R` :
```bash
sbatch -p wflowq -t 12:00:00 --mem-per-cpu=124GB  -J circosplot--wrap="module load system/R-4.2.1_Miniconda3; Rscript circlize_v2.R 100000 'zeros_scaled_filtered_bacterie-100ng-1_S20_L004_R1_001_unmerged.bedgraph, zeros_scaled_filtered_bacterie-100ng-2_S21_L004_R1_001_unmerged.bedgraph'"
```
