#!/usr/bin/perl -w

binmode STDIN,  ':encoding(UTF-8)';
binmode STDOUT, ':encoding(UTF-8)';
binmode STDERR, ':encoding(UTF-8)';

=pod

=head1 NAME

 data_prepare.pl
 
=head1 DESCRIPTION

 Prend en entrée un dossier de sortie de demultiplexage
 Et prepare les donnees pour le lancement de NextFlow
 
=head1 SYNOPSIS

 data_prepare.pl -h | -i

=head1 OPTIONS

 -i|s : Chemin vers le dossier a preparer -> Obligatoire
 
=head1 EXEMPLES

 perl extractReads.pl -i /illumina/MiSeq/analyzed/230228_M01945_0423_000000000-GFKVR

=head1 DEPENDENCIES

 - Web service permettant la recuperation des adresses mails a partir de l'id 
   http://vm-esitoul.toulouse.inra.fr:8080/jaxws-plaGeDataFileParser/adminWS

=head1 AUTHOR

 Jules Sabban pour Plateforme genomique Toulouse (get-plage.bioinfo@genotoul.fr)
 
=encoding UTF8 

=cut

#############################################################################################################################
#																															#
#				LIBRAIRIES																									#
#																															#
#############################################################################################################################
use strict;
use File::chdir;
use Getopt::Long;
use SOAP::Lite;
use File::Copy "cp";
use Switch;
use File::Basename;
use utf8;

#############################################################################################################################
#																															#
#				INITIALISATION																								#
#																															#
#############################################################################################################################
my $input = '';
my $mail_dev = 'jules.sabban@inrae.fr';

GetOptions( 
	"i=s" => \$input
);

if ($input eq '') {
	print STDERR "USAGE : data_prepare.pl -i <dir_path>";
	die;
}
##################################################################
#
#							MAIN
#
##################################################################
MAIN:
{
    my $checkTest = $input =~ m/.*data_test.*/ ? 1 : 0;

	my $nf_outputs_folder = '/home/sbsuser/work/Nextflow/wf-illumina-nf/results';
	my $wf_illumina_nf_folder_path = '/save/sbsuser/scripts-ngs/wf-Illumina-nf_Current';
	if ($checkTest){
		$wf_illumina_nf_folder_path = '/home/sbsuser/work/test/jules/VisualStudioSources/wf-illumina-nf';
	}

    my @RunDir = split("/", $input);
    my @RunInfo = ();
    @RunInfo = split("_", $RunDir[$#RunDir]);
    my $barcodeFlowcell;
	if ($RunInfo[3] =~ m/000000000-/){
		my @FCBarcode = split('-', $RunInfo[3]);
		$barcodeFlowcell = $FCBarcode[$#FCBarcode];
	} else {
		$barcodeFlowcell = $RunInfo[3];
	}
    my $lane = '1';
    if ($RunInfo[4] =~ m/Lane(\d)/){
        $lane = $1;
    }

    chdir $input or (print STDERR "Impossible de se déplacer dans $input\n" and die);

    print ("Recherche de la jFlow\n");
    my $regexpSampleSheetjFlow = '^[0-9]{8}_.*_jFlow.*\.toSubmit$';
    my $file_jflow = `ls -t | egrep $regexpSampleSheetjFlow  | head -1`; $? and (print STDERR "[Erreur]Récup de la derniere jflow\n" and die);
    chomp($file_jflow);
    print ("\tjFlow trouvée : $file_jflow\n");

    print ("\tParametrage de la commande Nextflow\n");
	my $submitted_jflow = $file_jflow;

	# parametrage pour NGL
	my $regexpRunCreated = '^RunNGL-Bi.created$';
	my $file_run_bi = `ls -t | egrep $regexpRunCreated  | head -1`; $? and (print STDERR "[Erreur]Récup du fichier $regexpRunCreated\n" and die);
	chomp($file_run_bi);
	print ("\tFichier run NGL-Bi trouvé : $file_run_bi\n");
	open (RUNBI, $file_run_bi);
	my $ngl_bi_run_name = <RUNBI>;
	chomp($ngl_bi_run_name);
	close (RUNBI);

	my $sq_xp_code = `grep 'Experiment Name' SampleSheet.csv | cut -d',' -f2`; $? and print STDERR "[Erreur]Récup du code experience NGL-SQ\n";
	chomp($sq_xp_code);
	if ( length($sq_xp_code) <= 3 ) {
		undef($sq_xp_code);
	}

	# ouverture de la jFlow
	print ("Ouverture en lecture de : $submitted_jflow\n");
	open(SJF,"< $submitted_jflow") or (print STDERR "Impossible d'ouvrir le fichier jflow $submitted_jflow\n" and die);
	while (my $line = <SJF>){
		print "Lecture ligne $.\n";
		next if ($line =~ /(^$)|(^#.*$)/);		# exclusion des lignes vide ou commençant par #
		my ($dataNature) =  $line =~ m/--data-nature \'?(\S+)\'?/;
		if ($dataNature eq 'ReadyToLoad') {
			my @jflow_components = split(' ', $line);
			my $jflow_pipeline = $jflow_components[2];
			switch($jflow_pipeline) {
				case 'illumina_rnaseq' { $dataNature = 'RNA-Stranded'}
				case 'illumina_qc' { $dataNature = 'DNA'}
				case 'illumina_diversity_qc' { $dataNature = 'DNA'}
				case 'methylseq' { $dataNature = 'methylated'}
				case 'illumina_10X_qc' { $dataNature = '10X'}
				else { $dataNature = 'unknown'}
			}
		}
		print ("Analyses NextFlow lancées sur données de type : $dataNature.\n");
		my ($project) = $line =~ m/--project-name (\S+)/;
		my (@samples) = $line =~ m/select-sample-id=(\S+)/g;
		my ($isMultiplex) = $#samples == 0 ? 'false' : 'true';
		my ($genomeRef) = $line =~ m/--reference-genome (\S+)/;
		my ($transcriptomeRef) = $line =~ m/--reference-transcriptome (\S+)/;
		my ($runName) = $line =~ m/--name \'(\S+)\'/;
		my ($sequencerLong) = $line =~ m/--sequencer \'(\S+)\'/;
		my ($sequencer, $sequencerID) = split('-', $sequencerLong);
		my ($date) = $line =~ m/--date \'(\S+)\'/;
		my ($fcID, $laneNumber, $demuxUniqueness) = $RunDir[$#RunDir] =~ m/\d{6}_\S{6}_\d{4}_(\S+)_Lane(\d)_(.*)$/;
		my ($fcType) = $line =~ m/--type \'(.{0,25}Lane.?\d{1})\'/;
		my ($description) = $line =~ m/--description \'(.+)\'/;
		my ($species) = $line =~ m/--species \'(\S+)\'/;
		my ($minOverlap) = $line =~ m/--min_overlap (\d+)/;
		my ($maxOverlap) = $line =~ m/--max_overlap (\d+)/;

		# Qui sera le destinataire principal du mail nextflow ?
		my $nf_mailRecipient = "";
		eval {
			$nf_mailRecipient = get_mail_by_ident( get_operator_from_sample_sheet( $input."/SampleSheet.csv" ) );
		};
		{
			if ($@) {
				print ("\t$@\n") ;
				$nf_mailRecipient = $mail_dev;
			}
		}
		print ("Le destinataire principal des mails NF est : $nf_mailRecipient\n");

		# Avoid colision between STAR versions
		my $force_indexing = 0;
		if ($dataNature eq 'RNA-Stranded' && defined($genomeRef)) {
			print "Verification de la version de STAR utilisée lors de l'indexation du génome de ref.\n";
			my $genomeDirName = dirname($genomeRef);
			my $index_version = `grep versionGenome "$genomeDirName/genomeParameters.txt" | cut -d\$'\t' -f2`;
			chomp($index_version);
			print("\tRecherche de la version $index_version de STAR dans la config de Nextflow\n");
			my $nf_star_version = `grep "bioinfo/STAR-$index_version" "$wf_illumina_nf_folder_path/conf/base.config" | wc -l`;
			print "nf star version : $nf_star_version";
			if ($nf_star_version == 0) {
				print "\tLa version STAR utilsée pour l'indexation du genome est différente de celle utilisée par NextFlow. L'index sera refait.\n";
				$force_indexing = 1;
			} else {
				print "\tLa version STAR utilsée pour l'indexation du genome est la même que celle utilisée par NextFlow. L'index est conservé.\n";
			}
		}

		my $host = hostname;
		my $nf_host = $host =~ /node\d{3}/ ? 'genologin' : 'genobioinfo';

		my $nf_file_prefix = "${.}_${project}_${runName}";
		my $nf_params_file = "${input}/${nf_file_prefix}_params.yml";
		open (NF_PARAMS,"> $nf_params_file") or (print STDERR "Impossible d'ouvrir le fichier $nf_params_file : $!\n" and die);

		print NF_PARAMS "inputdir: '$input'\n";
		print NF_PARAMS "project: '$project'\n";
		print NF_PARAMS "select_samples: '" . join(',', @samples) . "'\n";
		print NF_PARAMS "is_multiplex: $isMultiplex\n";
		print NF_PARAMS "data_nature: '$dataNature'\n";
		print NF_PARAMS "species: '$species'\n";
		print NF_PARAMS "reference_genome: '$genomeRef'\n" if (defined($genomeRef));	# parametre non obligatoire
		print NF_PARAMS "make_star_index: true\n" if ($force_indexing);	# parametre non obligatoire
		print NF_PARAMS "reference_transcriptome: '$transcriptomeRef'\n" if (defined($transcriptomeRef));	# parametre non obligatoire
		print NF_PARAMS "run_name: '$runName'\n";
		print NF_PARAMS "sequencer: '$sequencer'\n";
		print NF_PARAMS "machine_id: '$sequencerID'\n";
		print NF_PARAMS "run_date: '$date'\n";
		print NF_PARAMS "fc_id: '$fcID'\n";
		print NF_PARAMS "fc_type: '$fcType'\n";
		print NF_PARAMS "lane: '$laneNumber'\n";
		print NF_PARAMS "insert_to_ngl: false\n" unless (defined($sq_xp_code));
		print NF_PARAMS "sq_xp_code: '$sq_xp_code'\n" if (defined($sq_xp_code));	
		print NF_PARAMS "bi_run_code: '$ngl_bi_run_name'\n" if (defined($ngl_bi_run_name));	
		print NF_PARAMS "min_overlap: $minOverlap\n" if (defined($minOverlap));	# parametre non obligatoire
		print NF_PARAMS "max_overlap: $maxOverlap\n" if (defined($maxOverlap));	# parametre non obligatoire
		print NF_PARAMS "email: '$nf_mailRecipient'\n" if (defined($nf_mailRecipient));	# parametre non obligatoire
		print NF_PARAMS "host: '$nf_host'\n";
		print NF_PARAMS "description: '$description'\n";

		close NF_PARAMS;
		print ("Le fichier de parametres nextflow est ici : $input/$nf_params_file\n");

		print ("\tCopie du fichier de config FASTQSCREEN dans le répertoire courant\n" );
		unless(-e $input."/fastq_screen.conf"){
			cp($wf_illumina_nf_folder_path."/assets/fastq_screen.conf_example", $input."/fastq_screen.conf") or 
			print STDERR "Impossible de copier le fichier fastq_screen.conf_n" and die;
		}
		my $local_run_folder = basename($input);
		unless(-e "${nf_outputs_folder}/${local_run_folder}"){
			mkdir "${nf_outputs_folder}/${local_run_folder}" or (print STDERR "Impossible de créer le répertoire ${nf_outputs_folder}/${local_run_folder}" and die);
		}
		unless(-e "${input}/nextflow"){
			symlink("${nf_outputs_folder}/${local_run_folder}", "${input}/nextflow" ) or (print STDERR "Impossible de créer le symlink ${input}/nextflow" and die);
		}
		my $nextflow_profile = $checkTest ? 'dev' : 'prod';
		my $nextflow_time_job = $checkTest ? '3:00:00' : '3-00';
		my $nextflow_cmd_line_sbatch = "sbatch -p wflowq -t $nextflow_time_job --mem 5GB -o ${nf_file_prefix}.out";
		$nextflow_cmd_line_sbatch .= "-J nf-illumina_${barcodeFlowcell}_${lane}_$. --wrap=";

		my $nextflow_cmd_line_wrap = "module load bioinfo/nfcore-Nextflow-v22.12.0-edge; ";
		$nextflow_cmd_line_wrap .= "cd $input/nextflow; ";
		$nextflow_cmd_line_wrap .= "nextflow run $wf_illumina_nf_folder_path/main.nf -ansi-log false ";
		$nextflow_cmd_line_wrap .= "-profile $nextflow_profile ";
		$nextflow_cmd_line_wrap .= "-params-file ${input}/${nf_file_prefix}_params.yml ";
		$nextflow_cmd_line_wrap .= "-w ./work/${nf_file_prefix} ";

		my $nextflow_cmd_line = "$nextflow_cmd_line_sbatch\" $nextflow_cmd_line_wrap \" ";
		print ("Commande nextflox à lancer : $nextflow_cmd_line\n");

		chdir "$input/nextflow" or (print STDERR "Impossible de se déplacer dans $input/nextflow" and die);
		open(NFC,"> $input/nextflow/${nf_file_prefix}_nextflow.cmd")
			and print("La commande nextflow est stockée ici : $input/nextflow/${nf_file_prefix}_nextflow.cmd\n")
			or (print STDERR "Impossible d'ouvrir en écriture le fichier : ${nf_file_prefix}_nextflow.cmd\n" and die);
		print NFC "$nextflow_cmd_line\n";
		close NFC;
		print "\n";
	}
	close SJF;
}
#############################################################################################################################
#																															#
#				FONCTIONS																								#
#																															#
#############################################################################################################################
=head2 function get_mail_by_ident

	Title		 : get_mail_by_ident
	Usage		 : $user_mail = get_mail_by_ident( $user_ident )
	Prerequisite : Un web service qui permet d'interroger la base de donnees PlaGe.
	Function	 : Retourne l'identifiant de l'utilisateur.
	Returns	     : String
	Args		 : $user_ident  String - Ident de la personne.
	Globals	     : none

=cut

sub get_mail_by_ident {
	my ( $ident ) = @_ ;
	my $result = 0 ;
	
	my $client = SOAP::Lite->proxy("http://genomique.genotoul.fr:8080/jaxws-plaGeDataFileParser/adminWS")->default_ns('http://genomique.genotoul.fr/')->soapversion('1.1')->envprefix('soapenv')->readable('true'); 
	#my $client = SOAP::Lite->proxy("http://esitoul-dev.toulouse.inra.fr:8080/jaxws-plaGeDataFileParser/adminWS")->default_ns('esitoul-dev.toulouse.inra.fr')->soapversion('1.1')->envprefix('soapenv')->readable('true'); 
	
	$result = $client->call( 'getPeopleDTO', SOAP::Data->name('peopleIdentifier')->value($ident) ); $? and (print  ("WARNING : echec de l'envoi du mail\n"));
	if( $result->fault ) {
		print ("WARNING : $result->faultstring\n") ;
		return "0";
	}
	if ($result->body && $result->body->{'getPeopleDTOResponse'}) {
		return $result->body->{'getPeopleDTOResponse'}{'return'}{'mail'};
	}
}

=head2 function get_operator_from_sample_sheet

 Title		  : get_operator_from_sample_sheet
 Usage		  : $operator_ident = get_operator_from_sample_sheet($sample_sheet_path)
 Prerequisite : none
 Function	  : Retourne l'identifiant de l'operateur du run.
 Returns	  : String
 Args		  : $sample_sheet_path  String - Chemin de la sample sheet.
 Globals	  : none

=cut

sub get_operator_from_sample_sheet {
	my ($sample_sheet_path) = @_ ;
	my $operator = "" ;
	open( my $FH_SAMPLESHEET, $sample_sheet_path ) or (print  ("WARNING : Impossible d'ouvrir la sample sheet '".$sample_sheet_path."'\n"));
	
	##Parsing Sample Sheet IEM
	while(my $ligne = <$FH_SAMPLESHEET>){
		chomp($ligne);
		if($ligne =~ /Investigator Name/){
			my @list = split(/,/,$ligne);
			
			#Récupérer l'opérateur
			$operator = $list[1];
			if($operator =~ /\r/){
				$operator = substr($operator,0,(length($operator)-1));
			}
			print  ("Opérateur : $operator\n");
			last; #we do not need to continue the loop
		}
	}
	close( $FH_SAMPLESHEET );
	return $operator ;
}
