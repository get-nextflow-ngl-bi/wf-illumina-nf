#!/bin/bash
#SBATCH --mail-user=jules.sabban@inrae.fr
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH -p wflowq
#SBATCH -t 4-00
#SBATCH --mem-per-cpu=12G
#SBATCH -e %x_%j.err
#SBATCH -o %x_%j.log

#### USAGE ###
<< usageMessage
USAGE : sbatch -J make_bedgraph_bacterium --array=1-6 make_bedgraph.sh <bam_fodler> <names_of_chromosomes_file> <chrom_pattern_to_remove>
EXAMPLE : sbatch -J make_bedgraph_pic --array=1-6make_bedgraph.sh ../samtools ../chrom_names "JANXI\|CM"

<chrom_pattern_to_remove> is mandatory, but can be a void string
usageMessage

#### ARGUMENT ####
I_DIR=$1 # path to samtools outputs
I_NAMES=$2	# path to chrom_names file
R_PATTERN=$3	# chr pattern to remove from bedgraph file

#### MODULES ####
module load bioinfo/samtools/1.18
module load bioinfo/bedtools/2.27.1

replace_chr_names() {
	# replace chr names
	echo -e "Replace chr names"
	SAMTOOLS_CMD="samtools view -H ${BAM_PATH} |"
	while read LINE
	do
		read -r OLD	NEW <<< $(echo -e $LINE)
		SAMTOOLS_CMD+=" sed -e 's/SN:${OLD}/SN:${NEW}/' |"
	done < $I_NAMES

	SAMTOOLS_CMD+=" samtools reheader - $BAM_PATH > filtered_${S_NAME}.bam"
	# note the - is on purpose, -c adds chr in front
	sh -c "$SAMTOOLS_CMD"
}



#samtools index chr_${S_NAME}.bam
#cp chr_${S_NAME}.bam filtered_${S_NAME}.bam

# filter out unplaced contigs
#samtools view  chr_${S_NAME}.bam `seq 1 18` X Y -b > filtered_${S_NAME}.bam

index_bam(){
	echo -e "Indexing filtered BAM"
	samtools index filtered_${S_NAME}.bam
}


# no longer need intermediary chr renamed bam/bai
#rm chr_${S_NAME}.bam chr_${S_NAME}.bam.bai

make_bedgraph(){
	# Scale factor reads per million (of total reads or chr mapped reads)
	scale=`bc <<< "scale=6;1000000/$(samtools view -f 0 -c filtered_${S_NAME}.bam)"`
	#0.000808
	echo -e "Scaling factor ${scale}. On to bedgraph generation"

	# bedgraph
	bedtools genomecov -ibam filtered_${S_NAME}.bam -bga -scale ${scale} > zeros_scaled_${S_NAME}.bedgraph
}

remove_unwanted_scaffold(){
	# Even though bam was filtered, still have 0 values for unplaced scaffolds...remove non numeric or X/Y chromosomes
	if [[ ! -z $R_PATTERN  ]]
	then
		grep -v $R_PATTERN zeros_scaled_${S_NAME}.bedgraph > zeros_scaled_filtered_${S_NAME}.bedgraph
		rm zeros_scaled_${S_NAME}.bedgraph
	else
		mv zeros_scaled_${S_NAME}.bedgraph zeros_scaled_filtered_${S_NAME}.bedgraph
	fi
}



main() {
	BAM=$(find $I_DIR -type f -name '*unmerged.bam' -execdir basename '{}' ';'|sed -n ${SLURM_ARRAY_TASK_ID}p)
	echo -e "Traitement de ${BAM}"
	BAM_PATH="${I_DIR}/${BAM}"

	S_NAME=$(basename $BAM .bam)

	replace_chr_names

	index_bam

	make_bedgraph

	remove_unwanted_scaffold
}

main
