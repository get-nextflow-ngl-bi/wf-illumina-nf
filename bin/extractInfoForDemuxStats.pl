#!/usr/bin/perl -w
binmode STDIN,  ':encoding(UTF-8)';
binmode STDOUT, ':encoding(UTF-8)';
binmode STDERR, ':encoding(UTF-8)';

=head1 NAME

 extractInfoForDemuxStats.pl
 
=head1 DESCRIPTION

 Extract from the samplesheet of lane : (1) sample names and (2) how many index are associated. Ecriture dans un fichier .indexNumber
 
=head1 SYNOPSIS

 extractInfoForDemuxStats.pl --sampleSheet

=head1 OPTIONS

 -sampleSheet|s : the samplesheet file
 
=head1 EXEMPLES

 perl extractInfoForDemuxStats.pl --sampleSheet 20210722_NOVASEQ6000_IEM_H3GHCDRXY_Lane1.csv 

=head1 AUTHOR

 Jules Sabban pour Plateforme genomique Toulouse (get-plage.bioinfo@genotoul.fr)
 
=cut

###################################################################
#
#						LIBRAIRIES
#
###################################################################
use strict;
use Getopt::Long;
use utf8;

###################################################################
#
#						INITIALISATION
#
####################################################################
my $sampleSheet="";

GetOptions ('sampleSheet=s' => \$sampleSheet,
);

if ($sampleSheet eq "") {
	print STDERR ("Please, give a file !");
	print STDERR ("USAGE : extractInfoForDemuxStats.pl --sampleSheet <File>\n");
	exit 0;
}

#Lane,Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,Sample_Project,Description
#Lane,Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,I5_Index_ID,index2,Sample_Project,Description

# recuperer le nombre de fois où "*Index_ID" est écrit et leur position
# récupere la position du sample_ID
#Pour chaque ligne recupérer le ou les index_ID
#Si index_ID =~ XX-XX-XX alors #index = 4
#Sinon #index = 1
#Faire la somme des #index par ligne
#Ecrire le nom de l'échantillon et le nombre d'index associé
#Ne pas oublier l'entete du fichier de sortie


### Lecture de la samplesheet :
open (my $handle, '<', $sampleSheet) or exit 1;
chomp(my @lines = <$handle>);
close $handle;

my $projectName="";
my $sample_ID_position;
my @index_ID_position=();
my %sample_info=();

my $machineName = 'MISEQ';	# evite comparaison avec chaine vide

my %regexForDataHeader = ();
$regexForDataHeader{'MISEQ'} = '^Sample_ID';
$regexForDataHeader{'NOVASEQ'} = '^Lane';

my %regexForSampleLine = ();
$regexForSampleLine{'MISEQ'} = '^.+,.+,.*,.*,.+,.+,.+,.*$';
$regexForSampleLine{'NOVASEQ'} = '^(\d),';

foreach my $line (@lines) {
	my @cur_line = split(',', $line);

	# Recherche de la machine
	if ($line =~ /^Description/) {
		$machineName = $cur_line[1];
		$machineName = $machineName =~ /^NOVASEQ/ ? 'NOVASEQ' : $machineName;
	}

	# Recherche des positions des Sample_ID et des Index_ID
	elsif ($line =~ m/${regexForDataHeader{$machineName}}/) {
		while ( my ( $indice, $valeur ) = each @cur_line ) { 
			if ($valeur eq "Sample_ID") { $sample_ID_position=$indice;}
			if ($valeur =~ /Index_ID$/) { push(@index_ID_position, $indice);}
		}
	}

	# Association Sample_ID avec son nombre d'index
	elsif ($line =~ m/${regexForSampleLine{$machineName}}/) {
		my $sample_ID = $cur_line[$sample_ID_position];
		my $index_number=0;
		my @cur_index_ID = ();
		foreach my $pos (@index_ID_position) {
			if ($cur_line[$pos] =~ /^SI-T|NT-\w{2}$/) { 
				$index_number = 2;
			} elsif ($cur_line[$pos] =~ /^\w{2}-\w{2}-\w{2}$/) {
				$index_number = 4;
			} else { 
				$index_number += 1; 
			}
		}
		$sample_info{$sample_ID} = $index_number;
	}
}

# ecriture du fichier de sortie :
my $content ="";
$content.="Sample\tNumberOfIndex\n";
foreach my $k (keys(%sample_info)) {
   $content.="$k\t$sample_info{$k}\n";
}

my $file2write = "indexNumber.tsv";

open(my $fh, '>', $file2write) or exit 1;
print $fh $content;
close $fh;




