TAG=$1
FASTP_REPORT=$2
QUALIMAP_REPORT=$3/genome_results.txt
GCBIAS_REPORT=$4

O_STAT="./${TAG}.stat"
O_CSV="./${TAG}.csv"

## Get values
DUPLI=$(jq '.duplication.rate' $FASTP_REPORT)

TOT_SEQ=$(( $(sed -n 's/number of reads = \(.*\)/\1/p' $QUALIMAP_REPORT | sed 's/ //g' | sed 's/,//g') / 2 ))
INSERT=$(sed -n 's/median insert size = \(.*\)/\1/p' $QUALIMAP_REPORT | sed 's/ //g')
GC_PERCENT=$(sed -n 's/GC percentage = \(.*%\)/\1/p' $QUALIMAP_REPORT | sed 's/ //g')
GEN_COV=$(grep ">= 1X" $QUALIMAP_REPORT | sed -n 's/There is a \(.*%\) of.*/\1/p' | sed 's/ //g')
MEAN_COV=$(sed -n 's/mean coverageData.*= \(.*X\)/\1/p' $QUALIMAP_REPORT | sed 's/ //g')
ALIGN=$(sed -n 's/number of mapped reads =.*(\(.*%\))/\1/p' $QUALIMAP_REPORT | sed 's/ //g')

AT_DROPOUT=$(grep '^ACCUMULATION_LEVEL' -A 1 $GCBIAS_REPORT | cut -d$'\t' -f6 | tail -1)
GC_DROPOUT=$(grep '^ACCUMULATION_LEVEL' -A 1 $GCBIAS_REPORT | cut -d$'\t' -f7 | tail -1)

## Write stat file
echo "duplication_rate: $DUPLI" >> $O_STAT
echo "total_sequences: $TOT_SEQ" >> $O_STAT
echo "mean_insert_size: $INSERT" >> $O_STAT
echo "GC_percent: $GC_PERCENT" >> $O_STAT
echo "genome_cov_percent: $GEN_COVcat " >> $O_STAT
echo "mean_cov: $MEAN_COV" >> $O_STAT
echo "align_percent: $ALIGN" >> $O_STAT
echo "GC_bias_low: ${AT_DROPOUT}%" >> $O_STAT
echo "GC_bias_high: ${GC_DROPOUT}%" >> $O_STAT

## Write export file
echo "Sample;Tot_seq;Duplication_rate;Mean_insert_size;%GC;%Genome_cov;Mean_cov;%Align;GC_bias_low;GC_bias_high" > $O_CSV
echo "$TAG;$TOT_SEQ;$DUPLI;$INSERT;$GC_PERCENT;$GEN_COV;$MEAN_COV;$ALIGN;${AT_DROPOUT}%;${GC_DROPOUT}%" >> $O_CSV