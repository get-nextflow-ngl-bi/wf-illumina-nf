#!/usr/bin/perl -w
binmode STDIN,  ':encoding(UTF-8)';
binmode STDOUT, ':encoding(UTF-8)';
binmode STDERR, ':encoding(UTF-8)';

=head1 NAME

 extractInfoForReaSets.pl
 
=head1 DESCRIPTION

 Extract (from samplesheet and RunNGL-Bi.created) and emit relevant informations for readSets creation
 
=head1 SYNOPSIS

 extractInfoForReaSet.pl --sampleSheet --runNGLBi

=head1 OPTIONS

 -sampleSheet|s : the samplesheet file
 -runNGLBi|s : the RunNGL-Bi.created file
 
=head1 EXEMPLES

 perl extractInfoForReaSet.pl --sampleSheet 20210607_NOVASEQ6000_BULKDEMUX_HFMH7DRXY.csv --runNGLBi RunNGL-Bi.created

=head1 AUTHOR

 Jules Sabban pour Plateforme genomique Toulouse (get-plage.bioinfo@genotoul.fr)
 
=cut

###################################################################
#
#						LIBRAIRIES
#
###################################################################
use strict;
use Getopt::Long;
use utf8;

###################################################################
#
#						INITIALISATION
#
###################################################################
my $sampleSheet="";
my $runNGLBiFile="";

GetOptions ('samplesheet=s' => \$sampleSheet,
			'runNGLBi=s'=> \$runNGLBiFile,
);

if ($sampleSheet eq "" || $runNGLBiFile eq "") {
	print STDERR ("At least one argument is missing !");
	print STDERR ("USAGE : extractInfoForReaSet.pl --sampleSheet <File> --runNGLBi <File>\n");
	exit 0;
}

my $laneNumber;
my $experimentName;
my $runName;
my $content;
my $file2write="readSetCreation.info";

###################################################################
#
#						MAIN
#
###################################################################
## Extract informations from files
### SamplSheet
#### ExperimentName
my $experimentName_ligne = `grep "Experiment Name" $sampleSheet | head -1`;
($experimentName) = $experimentName_ligne =~ m/Experiment Name,(.+)$/;

#### LaneNumber

if ($sampleSheet =~ "_MISEQ_") {
	$laneNumber = "1";
} else {
	open (my $handle, '<', $sampleSheet) or exit 1;
	chomp(my @lines = <$handle>);
	close $handle;
	
	foreach my $line (@lines) {
		if ($line =~ m/^(\d),/) {
			($laneNumber) = $line =~ m/^(\d),/;
			last;
		}
	}
}
### RunNGL-Bi.created
$runName = `cat $runNGLBiFile`;
chomp($runName);

## Write exit file
$content.="ExperimentName;$experimentName\n";
$content.="NGLBiRunName;$runName\n";
$content.="LaneNumber;$laneNumber\n";

open(my $fh, '>', $file2write) or exit 1;
print $fh $content;
close $fh;

