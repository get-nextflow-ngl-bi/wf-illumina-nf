#!/usr/bin/perl -w
binmode STDIN,  ':encoding(UTF-8)';
binmode STDOUT, ':encoding(UTF-8)';
binmode STDERR, ':encoding(UTF-8)';

=head1 NAME

 alignmentStatTreatment.pl
 
=head1 DESCRIPTION

 Lit les fichiers de sortie d'alignement et ajoute les informations extraites au treatment NGL-Bi
 
=head1 SYNOPSIS

 alignmentStatTreatment.pl --file <path>

=head1 OPTIONS

 --file=s : path to a stat file
 
=head1 EXEMPLES

 perl alignmentStatTreatment.pl --file /path/to/my/file.stat

=head1 AUTHOR

 Jules Sabban pour Plateforme genomique Toulouse (get-plage.bioinfo@genotoul.fr)
 
=cut

###################################################################
#
#						LIBRAIRIES
#
###################################################################
use strict;
use Getopt::Long;
use Log::Log4perl;

##################################################################
#
#						INITIALISATION
#
##################################################################
Log::Log4perl -> init('/home/sbsuser/save/scripts-ngs/NGL-Bi_client_Current/IG/SystemeInteractionNGL-Bi/conf/log4perl.conf');
my $logger = Log::Log4perl->get_logger("MyLog");

my $file = "";

GetOptions( 
	"file=s" => \$file, 	# path to statistic file
);

if ($file eq "") {
	$logger -> warn("USAGE : alignmentStatTreatment.pl --file <STAT_FILE>\n");
	$logger -> fatal("At least one argument is missing !") and die;
}

##################################################################
#
#							MAIN
#
##################################################################
MAIN:
{	
	# Initialisation du hash qui contiendra les info a inserer dans NGL-Bi
	my %TreatmentProperties = ();

	# Définitions des regex
	my $total_regex = '(\d+) .*in total';									# total regexp
	my $qcfailure_regex = '(\d+ \+ (\d+) in total)|((\d+) QC failure)';		# qcfailure regexp 
	my $duplicates_regex = '(\d+) .*duplicates';							# duplicates regexp 
	my $mapped_regex = '(\d+) .*mapped \(([^:]*).*\)';						# mapped regexp   
	my $paired_regex = '(\d+) .*paired in sequencing';						# paired regexp  	
	my $read1_regex = '(\d+) .*read1';										# read1 regexp   
	my $read2_regex = '(\d+) .*read2';										# read2 regexp    
	my $matemapped_regex = '(\d+) .*with itself and mate mapped';			# matemapped regexp   
	my $properlypaired_regex = '(\d+) .*properly paired \(([^:]*).*\)';		# properlypaired regexp    
	my $singletons_regex = '(\d+) .*singletons \(([^:]*).*\)';				# singletons regexp 
	my $mapch1_regex = '(\d+) .*with mate mapped to a different chr';		# mapch1 regexp
	my $supplementary_regex = '(\d+).*supplementary';						# supplementary regexp
    
    # Lecture du fichier de statistiques
	open my $openFile, '<', $file; $? and $logger -> fatal("Impossible d'ouvrir le fichier $file") and die;
	chomp( my @lines = <$openFile> );
	close $openFile;
	
	foreach my $line (@lines) {
		#$logger -> info("Evaluation de la ligne : ". $line);
		if ($line =~  qr/$total_regex/) {
			$TreatmentProperties{"total"} = $1;
			$logger -> info("total_regex a ete trouvee et vaut : ". $TreatmentProperties{"total"});
		}
		if ($line =~  qr/$qcfailure_regex/) {
			if ($2 ne '') {
				$TreatmentProperties{"qcfailure"} = $2;
			} else {
				$TreatmentProperties{"qcfailure"} = $4;
			}
			
			$logger -> info("qcfailure a ete trouvee et vaut : ". $TreatmentProperties{"qcfailure"});
		}
		if ($line =~  qr/$duplicates_regex/) {
			$TreatmentProperties{"duplicates"} = $1;
			$logger -> info("duplicates a ete trouvee et vaut : ". $TreatmentProperties{"duplicates"});
		}
		if ($line =~  qr/$mapped_regex/) {
			if (index($line,'primary') != -1) {
				$TreatmentProperties{"primary_mapped_nb"} = $1;
				$TreatmentProperties{"primary_mapped_perc"} = $2;
				$logger -> info("primary_mapped_nb a ete trouvee et vaut : ". $TreatmentProperties{"primary_mapped_nb"});
				$logger -> info("primary_mapped_perc a ete trouvee et vaut : ". $TreatmentProperties{"primary_mapped_perc"});
			} else {
				$TreatmentProperties{"mapped_nb"} = $1;
				$TreatmentProperties{"mapped_perc"} = $2;
				$logger -> info("mapped_nb a ete trouvee et vaut : ". $TreatmentProperties{"mapped_nb"});
				$logger -> info("mapped_perc a ete trouvee et vaut : ". $TreatmentProperties{"mapped_perc"});
			}
		}
		if ($line =~  qr/$paired_regex/) {
			$TreatmentProperties{"paired"} = $1;
			$logger -> info("paired a ete trouvee et vaut : ". $TreatmentProperties{"paired"});
		}
		if ($line =~  qr/$read1_regex/) {
			$TreatmentProperties{"read1"} = $1;
			$logger -> info("read1 a ete trouvee et vaut : ". $TreatmentProperties{"read1"});
		}
		if ($line =~  qr/$read2_regex/) {
			$TreatmentProperties{"read2"} = $1;
			$logger -> info("read2 a ete trouvee et vaut : ". $TreatmentProperties{"read2"});
		}
		if ($line =~  qr/$matemapped_regex/) {
			$TreatmentProperties{"matemapped"} = $1;
			$logger -> info("matemapped a ete trouvee et vaut : ". $TreatmentProperties{"matemapped"});
		}
		if ($line =~  qr/$properlypaired_regex/) {
			$TreatmentProperties{"properlypaired_nb"} = $1;
			$TreatmentProperties{"properlypaired_perc"} = $2;
			$logger -> info("properlypaired_nb a ete trouvee et vaut : ". $TreatmentProperties{"properlypaired_nb"});
			$logger -> info("properlypaired_perc a ete trouvee et vaut : ". $TreatmentProperties{"properlypaired_perc"});
		}
		if ($line =~  qr/$singletons_regex/) {
			$TreatmentProperties{"singletons_nb"} = $1;
			$TreatmentProperties{"singletons_perc"} = $2;
			$logger -> info("singletons_nb a ete trouvee et vaut : ". $TreatmentProperties{"singletons_nb"});
			$logger -> info("singletons_perc a ete trouvee et vaut : ". $TreatmentProperties{"singletons_perc"});
		}
		if ($line =~  qr/$mapch1_regex/ && index($line,'mapQ') == -1) {
			$TreatmentProperties{"mapch1"} = $1;
			$logger -> info("mapch1 a ete trouvee et vaut : ". $TreatmentProperties{"mapch1"});
		}
		if ($line =~  qr/$supplementary_regex/) {
			$TreatmentProperties{"supplementary"} = $1;
			$logger -> info("supplementary a ete trouvee et vaut : ". $TreatmentProperties{"supplementary"});
		}
	}


	## Insertion du treatment
	## TODO

}
$logger -> info("Fin normale du script.");

##################################################################
#
#						FUNCTIONS
#
##################################################################
































