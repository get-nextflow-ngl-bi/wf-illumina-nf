# get-nextflow-ngl-bi/wf-Illumina-nf: Usage

Below is a description of the configurable options available for the pipeline.

## Usefull core Nextflow arguments
> **NB:** These options are part of Nextflow and use a _single_ hyphen (pipeline parameters use a double-hyphen).

- **`-name`** [str]  
Name for the pipeline run. If not specified, Nextflow will automatically generate a random mnemonic.

- **`-resume`**    
Specify this flag when restarting a pipeline. Nextflow will used cached results from any pipeline steps where the inputs are the same, continuing from where it got to previously.  
You can also supply a run name to resume a specific run: `-resume [run-name]`. Use the `nextflow log` command to show previous run names.

- **`-work-dir / -w`** [str]  
Specify the directory where intermediate result files are stored. the default is `$launchDir/work/`.

- **`-profile`** [str]  
Use this parameter to choose a configuration profile. Profiles can give configuration presets for different compute environments.  
Several generic profiles are bundled with the pipeline which instruct the pipeline to use software packaged using different methods (Docker, Singularity, Podman, Conda) - see below.  
Note that multiple profiles can be loaded, for example: `-profile dev,docker` - the order of arguments is important!
They are loaded in sequence, so later profiles can overwrite earlier profiles.  
Availlable profiles:
    * `dev`
        * A profile with a complete configuration for automated testing
    * `prod`
        * Use in production context only (largest ressources)  
  

## Pipeline Options
The pipeline offers the following configurable options with their default values. It is possible to use them with double hyphens in the command line, but the pipeline need a lot of them, so to avoid a very long command line it is recommanded to put these options into a YAML parameter file (see example in the README.md), with `-params-file [path]`.

### Mandatory parameters
Some parameters have not default value, therefore they must be set to run the pipeline. Here, is the exhaustive list of them :    

- **`--inputdir`** [str]  
Path to the input directory for the data to be analyzed. No default value, MUST be given to the command line. This is the output directory of bcl2fastq. See bellow for the particular structure of it.  
_Default_ : null

- **`--project`** [str]  
The project name associated with the analysis. The value of this parameter MUST be a directory name found in the `inputdir` path.  
_Default_ : null

- **`--select_samples `** [list]  
Comma separated list of samples name. Each samples in this list must be exactly the beginning of the fastq file name in the `project` directory. If this option is empty, the workflow takes as input every reads fastq files in the directory. 
_Default_ : null

- **`--data_nature`** [str]  
Nature of the data sequenced. This parameter will be used to automatically select the workflow. Authorized values are : `DNA`, `RNA-*`, `Amplicon`, `16S`, (soon : `Emseq-DNA`, `Hi-C`, `sparse`).  
If value of data_nature is unknown, only the CORE pipeline is executed.  
_Default_ : null

- **`--sequencer`** [str]  
The sequencing platform used, such as `NovaSeq` or `AVITI`.  
_Default_ : null

- **`--is_multiplex`** [bool]  
Indicates if the data is multiplexed or not.  
_Default_ : false

- **`--run_name`** [str]  
The name of the analysis run, defined in NGL-SQ. Will be used among other things for the naming of some files.  
_Default_ : null

- **`--host`** [str]  
The name of the server on which the pipeline is launched. This value is used to select slurm modules to load.   
_Default_ : genobioinfo

- **`--shared_modules`** [str]  
Path to the shared_modules sources. This is nextflow modules shared between several pipelines.    
_Default_ : '/home/sbsuser/save/scripts-ngs/shared_modules_Current'  

- **`--ngl_bi_client`** [str]  
Path to NGL-Bi client source.  
_Default_ : '/home/sbsuser/save/scripts-ngs/NGL-Bi_client_Current'

- **`--insert_to_ngl`** [bool]  
Whether to insert data into NGL-Bi or not.  
_Default_ : true

- **`--sq_xp_code`** [str]  
Sequencing experiment code from NGL-SQ. Mandatory if `insert_to_ngl = true`.  
_Default_ : null

- **`--bi_run_code`** [str]  
Run code for NGL-Bi.  
_Default_ : null

### Optionnal parameters
Some other parameters are only for tracability and have no effect on analysis, there are :  
- **`--outdir`** [str]  
Path to the output directory. The real output directory is constructed as follow : `${params.inputdir}/nextflow/${params.project}/${params.run_name}_${nf_uniqueness}` if all these values are set or `${launchDir}/results_${nf_uniqueness}`. Where `${nf_uniqueness}` is the current date.  
This parameter is not intended to be set manually.  
_Default_ : `${launchDir}/results_${nf_uniqueness}`

- **`--machine_id`** [str]  
The machine identifier, such as `A00318` or `AV232702`.  
_Default_ : null

- **`--fc_type`** [str]  
Information about the flow cell used. Example : `Flowcell Standard - Lane 1`  
_Default_ : null

- **`--fc_id`** [str]  
The flow cell identifier/barcode.  
_Default_ : null

- **`--lane`** [str]  
The lane number(s).  
_Default_ : null

- **`--species`** [str]  
(Scientific) Name of the species sequenced.  
_Default_ : null

- **`--run_date`** [str]  
The date of the run, formatted as YYMMDD.  
_Default_ : null

- **`--description`** [str]  
The nG6 like description of the analysis.  
_Default_ : null

### Skipping parameters
There are some availlable flags can be set to not run some parts of the pipeline.  
- **`--no_subset`** [bool]  
To skip subsampling step in core pipeline.  
_Default_ : false

- **`--skip_core_illumina`** [bool]  
To skip core illumina sub-workflow in core pipeline. Have effect only if `sequencer` is NovaSeq or MiSeq.    
_Default_ : false

- **`--skip_core_element`** [bool]  
To skip core element sub-workflow in core pipeline.Have effect  only if `sequencer` is AVITI.    
_Default_ : false

## Workflows related parameters
Here are listed mandatory parameters used to performe one particular analysis. Most of these options have default value, so is it not needed to overwriting them all the time. 
### CORE
- **`--samplesheet`** [str]  
Path to the IEM sampleSheet, only use for CORE illumina sub-workflow.  
_Default_ : params.inputdir + "/SampleSheet.csv"

- **`--fastp_n_reads`** [num]  
Number of reads to process for duplicate estimation with FASTP (`--reads_to_process` parameter).  
_Default_ : 100000000

- **`--miseq_subset_seq`** [str]  
Number of sequences to use to subset reads for MiSeq dataset.  
_Default_ : 50000

- **`--nova_subset_seq`** [str]  
Number of sequences to use to subset reads for NovaSeq dataset.  
_Default_ : 50000000

- **`--large_sampling_threshold`** [int]  
Number of samples from which we consider that it is a very large multiplexed run. This option takes into account that the quantity of data for each sample is small.  
_Default_ : 200

- **`--large_indexing_nova_subset_seq`** [str]  
Number of sequences to use to subset reads for NovaSeq in case of very large multiplexed run.  
_Default_ : 500000


### DNA / RNA
- **`--reference_genome`** [str]  
Path to the reference genome.  
_Default_ : null

- **`--reference_transcriptome`** [str]  
Path to the reference transcriptome.  
_Default_ : null

- **`--make_star_index`** [bool]  
Whether to force to create a STAR index, for RNA analysis using a reference genome.  
_Default_ : false


### Amplicon / 16S
- **`--min_overlap`** [int]  
Minimum overlap for paired reads merging.  
_Default_ : 20

- **`--max_overlap`** [int]  
Maximum overlap for paired reads merging.  
_Default_ : 55

- **`--max_mismatch_density`** [float]  
Maximum mismatch density for paired reads merging (flash -x option).  
_Default_ : 0.1

- **`--assignation_databank`** [str]  
Path to the databank for taxonomic assignment.  
_Default_ : /save/ng6/TODO/HiSeqIndexedGenomes/new_struct/ncbi_16S/240319_release/16SMicrobial

- **`--blast_outfmt`** [int]  
BLAST output format.  
_Default_ : 7

- **`--blast_max_target`** [int]  
Maximum BLAST targets.  
_Default_ : 10

- **`--sortmerna_db_path`** [str]  
Path where every SortMeRNA databases are stored.  
_Default_ : '/usr/local/bioinfo/src/SortMeRNA/sortmerna-2.1b/rRNA_databases'


### 10X  
- **`--single_cell`** [bool]  
True to enable the 10X mode (index files support).  
_Default_ : false  

### MethylSeq
Not available yet.  
- **`--puc19`** [str]  
Path to the fasta of the pUC19 (methylated control).  
_Default_ : null

- **`--lambda`** [str]  
Path to the fasta of the lambda (unmethylated control).  
_Default_ : null

## Other parameters
- **`--max_memory`** [str]  
Maximum memory to launch sbatch jobs  
_Default_ : 250.GB

- **`--max_time`** [str]  
Maximum time to launch sbatch jobs  
_Default_ : 90.d

- **`--max_cpus`** [str]  
Maximum cpus to launch sbatch jobs  
_Default_ : 48

- **`--cluster_options`** [str]  
Option used to launch slurm jobs. Usefull to exclude some busy nodes for example.  
_Default_ : null

- **`--is_dev_mode`** [bool]  
Development mode flag, automatically set `true` using dev profil (see bellow).  
_Default_ : false

- **`--DTM_mode`** [bool]  
Set true to add some special process for DTM validation.  
_Default_ : false

- **`--email`** [bool/str]   
Set to false to not send email notification, or set to an email address to receive on.  
_Default_ : null

- **`--email_dev`** [str]   
Only one email used in dev mode.  
_Default_ : jules.sabban@inrae.fr

- **`--email_on_fail`** [str]   
Email address for failure notifications.  
_Default_ : jules.sabban@inrae.fr

- **`--email_bioinfo`** [str]  
Bioinformatics team email address.  
_Default_ : get-plage.bioinfo@genotoul.fr

- **`--email_labo`** [str]  
Laboratory email address. (Currently null)  
_Default_ : get-plage.labo@genotoul.fr

## Parameters overwritten in dev mode
Here are listed every parameters with overwrited default value in dev mode. These parameters should not be use in command line in most cases.  
- **`--ngl_bi_client`** set to '/home/sbsuser/work/test/jules/VisualStudioSources/ngl-bi_client/'  
- **`--shared_modules`** set to '/home/sbsuser/work/Nextflow/shared_modules/ExportSources_Jules/'  
- **`--is_dev_mode`** set to true  


## Structure of the inputdir
The `--inputdir` parameter has not default value, so it must be given in the command line. This is a path to a folder that must have a particular structure. The `inputdir` folder must have the minimum following data folder :

For every workflows, the inputdir folder must only have a directory with the name of the project. In this project folders (or subfolders bellow), fastq files must be gzipped. In the input folder, it must also have the fastq_screen configuration file (see example in assets/).
For the core illumina pipeline, the inputdir folder must also contains à Stats folder, with statistics files of bcl2fastq inside.
