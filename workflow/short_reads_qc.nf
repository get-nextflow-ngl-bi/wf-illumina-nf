#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

include { 	getSummary; 
			endOfPipelineEvents;
			getPipelineInfo 			} from "${baseDir}/lib/pipeline.groovy"
include { 	softwareVersionsToYAML		} from "${params.shared_modules}/lib/utils.groovy"
// -------------------------------------------------
// 					CHANNELS
// -------------------------------------------------
// Illumina's channels
ss_path = file(params.samplesheet)
demuxSummary_path = file(params.inputdir+"/Stats/DemuxSummaryF1L${params.lane}.txt")
demuxStatXML_path = file(params.inputdir+'/Stats/DemultiplexingStats.xml')
ch_ss = ss_path.exists() ? Channel.fromPath(ss_path) : Channel.empty()
ch_DemuxSummary = demuxSummary_path.exists() ? Channel.fromPath(demuxSummary_path) : Channel.empty()
ch_DemuxStatXML = demuxStatXML_path.exists() ? Channel.fromPath(demuxStatXML_path) : Channel.empty()

// Element's channels
runManifestJSON_path = file(params.inputdir+"/RunManifest.json")
indexAssigned_path = file(params.inputdir+"/IndexAssignment.csv")
indexUnassigned_path = file(params.inputdir+"/UnassignedSequences.csv")
ch_runManifestJSON = runManifestJSON_path.exists() ? Channel.fromPath(runManifestJSON_path) : Channel.empty()
ch_indexAssigned = indexAssigned_path.exists() ? Channel.fromPath(indexAssigned_path) : Channel.empty()
ch_indexUnassigned = indexUnassigned_path.exists() ? Channel.fromPath(indexUnassigned_path) : Channel.empty()

def SamplesBaseDir = params.sequencer =~ 'AVITI' ? 'Samples' : ''

// Get samples globPatterns
def sampleList = []
def indexFilesList = []
if (params.select_samples) {
	params.select_samples.tokenize(',').each { sample ->
		sampleList.add("${params.inputdir}/${SamplesBaseDir}/${params.project}/**" + sample +"_*_L00${params.lane}_R{1,2}{_*,*}.fastq.gz")
		indexFilesList.add("${params.inputdir}/${SamplesBaseDir}/${params.project}/**" + sample +"_*_L00${params.lane}_I{1,2}{_*,*}.fastq.gz")
	}
} else {
	System.out.println "Aucun échantillon selectionné, on les sélectionne tous"
	sampleList.add("${params.inputdir}/${SamplesBaseDir}/${params.project}/**_L00${params.lane}_R{1,2}{_*,*}.fastq.gz")
	indexFilesList.add("${params.inputdir}/${SamplesBaseDir}/${params.project}/**_L00${params.lane}_I{1,2}{_*,*}.fastq.gz")
}

// Get 10X index Files
ch_index=Channel
	.fromPath(indexFilesList)
	.map{$it -> [$it.simpleName, $it]}
	.unique()

// fastq one by one
ch_read=Channel
	.fromPath(sampleList)
	.map{$it -> [$it.simpleName, $it]}
	.unique()

// Channel of rRNA databases for sortmerna
ch_sortmerna_db = Channel.from(
	params.sortmerna_db_path + '/silva-bac-16s-id90.fasta',
	params.sortmerna_db_path + '/silva-bac-23s-id98.fasta',
	params.sortmerna_db_path + '/silva-arc-16s-id95.fasta',
	params.sortmerna_db_path + '/silva-arc-23s-id98.fasta',
	params.sortmerna_db_path + '/silva-euk-18s-id95.fasta',
	params.sortmerna_db_path + '/silva-euk-28s-id98.fasta',
)

ch_demux_stat_json = Channel.fromPath("${params.inputdir}/Stats/Stats.json")

mismatchNumber = params.sequencer == 'MiSeq'? 0 : 1
//banksForConta = params.addBankForConta ? params.genomesRefForConta << params.addBankForConta : params.genomesRefForConta

ch_mqc_config = Channel.fromPath("${baseDir}/assets/multiqc_config.yaml")
ch_mqc_logo = Channel.from("${baseDir}/assets/get_logo.png")

createDir = file(params.outdir).mkdir()
params.summary = getSummary()
params.summary.collect{k,v -> println "$k : $v"}
// -------------------------------------------------
// 					INCLUDES
// -------------------------------------------------
include {	CORE_ILLUMINA } from "$baseDir/sub-workflows/local/core_illumina.nf"
include {	CORE_ELEMENT } from "$baseDir/sub-workflows/local/core_element.nf"
include {	CORE		} from "$baseDir/sub-workflows/local/core_pipeline.nf"
include {	DNA_QC		} from "$baseDir/sub-workflows/local/dna_qc.nf"
include {	RNA_QC		} from "$baseDir/sub-workflows/local/rna_qc.nf"
include {	DIVERSITY_QC	} from "$baseDir/sub-workflows/local/diversity_qc.nf"
include { 	PARSE_REPORTS } from "$baseDir/modules/local/module_DTM.nf"
include {	NGL			} from "$baseDir/sub-workflows/local/ngl.nf"
include {	TREATMENT_DEMUXSTAT as TREATMENT_DEMUX_RUN;
			TREATMENT_DEMUXSTAT as TREATMENT_DEMUX_READSETS;
						} from "$baseDir/modules/local/module_NGL-Bi.nf"
include {	MULTIQC		} from "${params.shared_modules}/multiqc.nf"
include {	MQC_HEADER	} from "${params.shared_modules}/multiqc.nf"
include {	GCBIAS as GC_BIAS } from "${params.shared_modules}/gcbias.nf"
include {	workflow_summary as WORKFLOW_SUMMARY } from "${params.shared_modules}/workflow_summary.nf"

// -------------------------------------------------
// 					WORKFLOW
// -------------------------------------------------
workflow SHORT_READS_QC {
	ch_mqc = Channel.empty()
	ch_versions = Channel.empty()

	WORKFLOW_SUMMARY()

	if ( params.sequencer =~ "NovaSeq|MiSeq" ) {
		if (params.skip_core_illumina) {
			log.info "Illumina's sequencer but skipping CORE_ILLUMINA"
			fastq = ch_read
			demux_stats = channel.empty()
		} else {
			CORE_ILLUMINA(ch_ss, ch_DemuxStatXML, ch_DemuxSummary, ch_read)
			fastq = CORE_ILLUMINA.out.fastq
			ch_versions = ch_versions.mix(CORE_ILLUMINA.out.versions)
			demux_stats = CORE_ILLUMINA.out.demuxStat
		}
	}

	if ( params.sequencer =~ "AVITI") {
		if (params.skip_core_element) {
			log.info "Elembio's sequencer but skipping CORE_ELEMENT"
			demux_stats = Channel.empty()
			fastq = ch_read
		} else {
			CORE_ELEMENT(ch_runManifestJSON, ch_indexAssigned, ch_indexUnassigned)
			demux_stats = CORE_ELEMENT.out.demuxStat
			fastq = ch_read
		}
	}

	CORE(fastq)
	ch_versions = ch_versions.mix(CORE.out.versions)
	ch_mqc = ch_mqc.mix(
		CORE.out.fastqc_report.collect{it[1]}.ifEmpty([]),
		CORE.out.fastqscreen_report.collect{it[1]}.ifEmpty([]),
		CORE.out.fastp_report.collect{it[1]}.ifEmpty([])
	)

	if (params.data_nature =~ 'DNA|GENOMIC|WGS') {
		DNA_QC(CORE.out.subset_fastq
			.collect{it[1]}
			.flatten()
			.map { $it -> [ ($it.simpleName =~ /(.*)_R[1-2]_.*/)[0][1] , $it ] }
			.groupTuple()
		)
		ch_mqc = ch_mqc.mix(
			DNA_QC.out.qualimap_report.collect{it[1]}.ifEmpty([]),
			DNA_QC.out.flagstats_output.collect{it[1]}.ifEmpty([])
		)
		ch_versions = ch_versions.mix(DNA_QC.out.versions)

		// DTM process
		if (params.DTM_mode) {
			GC_BIAS(DNA_QC.out.bam, params.reference_genome)
			PARSE_REPORTS(CORE.out.fastp_report, DNA_QC.out.qualimap_report, GC_BIAS.out.summary)
		}

	} else if (params.data_nature =~ 'RNA*|TRANSCRIPTOMIC') {
		RNA_QC(CORE.out.subset_fastq, ch_sortmerna_db)
		ch_mqc = ch_mqc.mix(
			RNA_QC.out.align_results.collect{it[1]}.ifEmpty([]),
			RNA_QC.out.sortmerna_log.collect{it[1]}.ifEmpty([]),
			RNA_QC.out.qualimap_report.collect{it[1]}.ifEmpty([]),
		)
		ch_versions = ch_versions.mix(RNA_QC.out.versions)

	} else if (params.data_nature =~ "16S|AMPLICON|METAGENOMIC|METATRANSCRIPTOMIC") {
		DIVERSITY_QC(fastq
			.collect{it[1]}
			.flatten()
			.map { $it -> [ ($it.simpleName =~ /(.*)_R[1-2]_.*/)[0][1] , $it ] }
			.groupTuple()
		)	// les deux en meme temps !!!!

		ch_mqc = ch_mqc.mix(
			DIVERSITY_QC.out.histogram.collect{it[1]}.ifEmpty([]),
			DIVERSITY_QC.out.logs.collect{it[1]}.ifEmpty([])
		)
		ch_versions = ch_versions.mix(DIVERSITY_QC.out.versions)

	} else {
		System.out.println "Le QC des données ${params.data_nature} n'a pas de sub-workflow spécifique pour le moment."
		ch_mqc = ch_mqc.mix( Channel.empty() )
	}

	version_yaml = softwareVersionsToYAML(ch_versions)
    	.collectFile(
			storeDir: "${params.outdir}/pipeline_info",
			name: 'software_mqc_versions.yml',
			sort: true,
			newLine: true
		)
	
	MQC_HEADER(getPipelineInfo())
	ch_mqc_config = ch_mqc_config.mix(MQC_HEADER.out.yaml)
	ch_mqc = ch_mqc.mix(version_yaml)

	MULTIQC(
		ch_mqc.collect(),
		ch_mqc_config.collect(),
		ch_mqc_logo
	)

	if (params.insert_to_ngl){
		NGL(
			params.sequencer,
			params.run_name,
			params.bi_run_code,
			params.sq_xp_code,
			fastq,
			ch_index,
			MULTIQC.out.html,
			params.project,
			params.project_hash,
			params.single_cell,
			ch_demux_stat_json
		)

		// Add demultiplexStat treatments
		TREATMENT_DEMUX_RUN(NGL.out.run_code, demux_stats, params.lane)
		TREATMENT_DEMUX_READSETS(NGL.out.readsets_file, demux_stats, '')
	}
}

endOfPipelineEvents(params.summary)