----------------------------------------------------------------------------------
==================================================
------------------------------- get-nf workflow ----------------------------
	        S H O R T  R E A D S  - N F   P I P E L I N E    
     			      V$version
==================================================
----------------------------------------------------------------------------------

NextFlow Run Name : $runName  

Demultiplexing is over, the analysis started at $dateStart.

The analysis of the following sequencing is running :
- Project : $project
- Run : $run_name
- Data : $data_nature
- Sequencer : $sequencer
- FlowCell : $flowcell
- Lane : $lane
- Directory : $directory
<% if (xpNGLSq){ out << "- NGL-SQ Experiment : $xpNGLSq " } %>
<% if (runNGLBi){ out << "- NGL-Bi RunCode : $runNGLBi " } %>

The command used to launch the workflow was as follows :

  $commandLine
  
---
$name
$homePage
