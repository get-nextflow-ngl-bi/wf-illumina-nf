/*
*   SHORT READS PIPELINE FUNCTIONS
*/

// ----------------------------------
//              Includes    
// ----------------------------------
import java.text.SimpleDateFormat

include {
	helpMessage;
	printOptions;
	paramsValidation;
    customMailSend;
    sendFinalMail;
    get_workflow_info;
    createSummary;
} from "${params.shared_modules}/lib/utils.groovy"


// ----------------------------------
//        Variables Definition    
// ----------------------------------
SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

pipeline_info = workflow.manifest.name.split('/')
pipeline_group = pipeline_info[0]
pipeline_project = pipeline_info[1]
pipeline_techno = pipeline_project.split('-')[1]

emails_map = create_email_map() 

pipeline_options_map = [
    inputdir:           [default: '', optional: false, help: 'Path to the input directory [demultiplexing output directory]'],
    samplesheet:        [default: '$params.inputdir/SampleSheet.csv', optional: true, help: 'Path to IEM SampleSheet. Only for Illumina analysis'],
    outdir:             [default: launchDir + '/results' , optional: true, help: 'Path where results are stored. Do not modify it, its value is automatically set'],
    project:            [default: '', optional: false, help: 'Project\'s name'],
    project_hash:       [default: '', optional: true, help: 'Project\'s hash ID for NGL-Bi'],
    select_samples:     [default: '', optional: true, help: 'Comma separated list of samples name. Each sample in this list must match exactly the beginning of the fastq file name in the project directory. If this option is empty, the workflow takes as input every fastq files in the director'],
    sequencer:          [default: '', optional: false, help: 'Name of the sequencer [NovaSeq600, AVITI2, ...]'],
    machine_id:         [default: '', optional: true, help: 'Serial Number or unique identifier of the sequencer'],
    fc_id:              [default: '', optional: true, help: '[metadata] Identifier of the Flowcell used'],   // useless ??
    fc_type:            [default: '', optional: true, help: '[metadata] Type of the Flowcell'],    // useless ??
    lane:               [default: '', optional: false, help: 'Number of the lane'],
    data_nature:        [default: '', optional: true, help: 'ENA library strategy [AMPLICON, 16S, WGS, ...]. It will be use to select analysis to perform'],
    species:            [default: '', optional: true, help: '[metadata] Scientific name of the species'],
    is_multiplex:       [default: false, optional: true, help: 'true if several samples were sequenced on the same lane'],
    run_name:           [default: '', optional: false, help: 'Human readable identifier of the analysis'],
    run_date:           [default: '', optional: false, help: 'Use for file renamming. Format : DD/MM/YYYY or YYYYMMDD'],
    description:        [default: '', optional: true, help: 'NG6 description of the run'],     // useless ??
    fastp_n_reads:      [default: 100000000, optional: true, help: 'Number of reads to process using fastp'],
    no_subset:          [default: false, optional: true, help: 'Is a subset of reads in fastq must be done before QC'],
    large_sampling_threshold: [default: 200, optional: true, help: 'Number of samples from which we consider that the sequencing is highly multiplexed'],
    miseq_subset_seq:   [default: '50000', optional: true, help: 'Number of reads to subsampling on MiSeq run'],
    nova_subset_seq:    [default: '50000000', optional: true, help: 'Number of reads to subsampling on NovaSeq run'],
    large_indexing_nova_subset_seq: [default: '500000', optional: true, help: 'Number of reads to subsampling on highly multiplexed NovaSeq run'],
    aviti_subset_seq:   [default: '50000000', optional: true, help: 'Number of reads to subsampling on Aviti run'],
    //depth: [default: 0, optional: true, help: 'In subsampling, number of X to keep'],     // Not use for the moment
    reference_genome:   [default: '', optional: true, help: 'Path to the genome FASTA file'],
    reference_transcriptome: [default: '', optional: true, help: 'Path to the transcriptome FASTA file'],
    make_star_index:    [default: false, optional: true, help: 'Is the FASTA file must be indexed by star'],
    sortmerna_db_path:  [default: '/work/project/PlaGe/sortemerna_db', optional: true, help: 'Path to the directory where the sortmerna databases are'],
    min_overlap:        [default: 20, optional: true, help: 'For join pair step, minimum overlapping value [-m Flash option]'],
    max_overlap:        [default: 55, optional: true, help: 'For join pair step, maximum overlapping value [-M Flash option]'],
    max_mismatch_density: [default: 0.1, optional: true, help: 'For join pair step, maximum mismatch density [-x Flash option]'],
    assignation_databank: [default: '/save/ng6/TODO/HiSeqIndexedGenomes/new_struct/ncbi_16S/240319_release/16SMicrobial', optional: true, help: 'Path to 16S Microbial database'],
    blast_outfmt:       [default: 7, optional: true, help: 'Format of output for BLASTn'],
    blast_max_target:   [default: 10, optional: true, help: 'Maximum number of hits from BLASTn'],
    single_cell:        [default: false, optional: true, help: 'true is library was build with 10X kit'],
    puc19:              [default: '', optional: true, help: 'Path to the puC19 fasta for MethylSeq analysis'],
    lambda:             [default: '', optional: true, help: 'Path to the lambda fasta for MethylSeq analysis'],
    ngl_bi_client:      [default: '/home/sbsuser/save/scripts-ngs/shared_modules_Current', optional: true, help: 'Path to NGL-Bi_Client sources'],
    insert_to_ngl:      [default: true, optional: false, help: 'Should the data be stored on NGL ?'],
    bi_run_code:        [default: '', optional: true, help: 'NGL-Bi Run Code'],
    sq_xp_code:         [default: '', optional: true, help: 'NGL-SQ Experiment Code'],
    ng6_name:           [default: true, optional: false, help: 'Is files must be nammed using ng6 standard'],
    shared_modules:     [default: '/home/sbsuser/save/scripts-ngs/shared_modules_Current', optional: false, help: 'Path to Shared_modules sources'],
    max_memory:         [default: '500.GB', optional: false, help: 'Maximum amount of memory that can be used to launch a sbatch job'],
    max_time:           [default: '90.d', optional: false, help: 'Maximum amount of time that can be used to launch a sbatch job'],
    max_cpus:           [default: '48', optional: false, help: 'Maximum number of CPUs that can be used to launch a sbatch job'],
    default_label:      [default: 'Pipeline', optional: false, help: 'Default label for MultiQC'],
    read_stats_label:   [default: 'ReadStats', optional: false, help: 'Read Stats label for MultiQC'],
    duplicats_label:    [default: 'Duplicats', optional: false, help: 'Duplicats label for MultiQC'],
    contamination_search_label: [default: 'ContaminationSearch', optional: false, help: 'Contamination Search label for MultiQC'],
    join_pairs_label:   [default: 'JoinPairs', optional: false, help: 'Join Pairs label for MultiQC'],
    alignment_stats_label: [default: 'AlignmentStats', optional: false, help: 'Alignment Stats label for MultiQC'],
    cluster_options:    [default: '', optional: true, help: 'Sbatch options to pass to each job'],
    is_dev_mode:        [default: false, optional: false, help: 'Preset of some options'],
    DTM_mode:           [default: false, optional: false, help: 'Enable some process for DTM analysis'],
    email:              [default: '', optional: false, help: 'Main email adress for execution pipeline notifications'],
    email_on_fail:      [default: 'get-plage.bioinfo@genotoul.fr', optional: false, help: 'Email adress to notify execution pipeline errors'],
    email_bioinfo:      [default: 'get-plage.bioinfo@genotoul.fr', optional: true, help: 'Bioinformatics team email adress for execution pipeline notifications'],
    email_labo:         [default: 'get-plage.labo@genotoul.fr', optional: true, help: 'Biologists team email adress for execution pipeline notifications'],
    host:               [default: 'genobioinfo', optional: false, help: 'Name of the HPC where the pipeline is executed. Must have special config file in conf folder'],
    skip_core_illumina: [default: false, optional: false, help: 'To skip Illumina subworkflow'],
    skip_core_element:  [default: false, optional: false, help: 'To skip Elembio subworkflow'],
    help:               [default: false, optional: true, help: 'To print help message']
]

begin_email_fields = get_workflow_info(
    [
        subject_prefix: "[${params.sequencer}]",
        subject_sufix: params.inputdir.split('/')[-1],
        // version: workflow.manifest.version,
        // wfRunName: workflow.runName,
        run_name: params.run_name,
        runNGLBi: (params.bi_run_code ?: ''),
        xpNGLSq: (params.sq_xp_code ?: ''),
        project: params.project,
        sequencer: params.sequencer,
        flowcell: params.fc_id,
        lane: params.lane,
        data_nature: params.data_nature,
        directory: params.inputdir,
        // commandLine: workflow.commandLine,
        dateStart: format.format(new Date()),
    ]
)

// ----------------------------------
//        Functions Definition    
// ----------------------------------
def getPipelineInfo() {
    def map = [:]
    map['Analysis Name'] = params.run_name
    File analysis_file = new File(params.outdir + '/ngl/NGL-BiAnalysis.created')
    if (analysis_file.exists()) {
        map['Analysis Code'] = analysis_file.getString()
    } else {
        map['Analysis Code'] = '###ANALYSIS_CODE###'
    }
    map['Library Type'] = params.data_nature ?: ''
    map['Sequencing Type'] = params.is_multiplex ? 'Multiplex' : 'Simplex'
    def ref = params.reference_genome ?: params.reference_transcriptome?: ''
    File ref_source = new File(ref + '_source')
    if (ref_source.exists()) {
        map['Reference'] = ref_source.getText()
    }
    def seq = params.sequencer ?: ''
    def sn = params.machine_id ?: ''
    def platform = "$seq $sn"
    map['Sequencing Platform'] = platform ?: ''

    return map
}

def create_email_map() {
    def hash = [:]

    def email_main = ''
    def email_bioinfo = ''
    def email_labo = ''
    def email_failure = ''

    email_main = params.email ?: params.email_bioinfo?: ''
    email_bioinfo = params.email_bioinfo ?: ''
    email_labo = params.email_labo ?: ''
    email_failure = params.email_on_fail ?: ''

    if (params.is_dev_mode) {
        email_main = params.email_dev
        email_bioinfo = email_main
        email_labo = email_main
        email_failure = email_main
        log.info "DEV mode activated : overwriting every email adresses to $email_main"
    }

    hash.main = email_main
    hash.bioinfo = email_bioinfo
    hash.labo = email_labo
    hash.failure = email_failure

    return hash
}

def create_final_email_fields(formatted_date, summary) {
    // Get AnalysisCode
    def analysis_code = ''
    File analysis_file = new File(params.outdir + '/ngl/NGL-BiAnalysis.created')
    if (analysis_file.exists()) {
        analysis_code = analysis_file.text
    }

    // Get RunCode
    def run_code = ''
    if (params.insert_to_ngl) {
        if (params.bi_run_code) { run_code = params.bi_run_code }
        else {
            File run_file = new File(params.outdir + '/ngl/RunNGL-Bi.created')
            if (run_file.exists()) {
                run_code = run_file.text
            }
        }
    }

    return get_workflow_info(
        [
            subject_prefix: "[${params.sequencer}]",
            subject_sufix: params.inputdir.split('/')[-1],
            project: (params.project ?: ''),
            run: (params.run_name ?: ''),
            runNGLBi: run_code,
            analysisNGLBi: analysis_code,
            xpNGLSq: (params.sq_xp_code ?: ''),
            dateComplete: formatted_date,
            summary: (summary ?: [:])
        ]
    )
}

def create_error_email_fields(formatted_date) {
    return get_workflow_info(
        [
            subject_prefix: "[${params.sequencer}]",
            subject_sufix:  params.inputdir.split('/')[-1] + " : ERROR",
            project:        params.project,
        ]
    )
}

def endOfPipelineEvents(summary) {
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
    end_mail_sent = false
    
    workflow.onComplete {
        emails_map = create_email_map()
        if (emails_map.main && emails_map.failure && !workflow.success) {
            emails_map.main = emails_map.failure
        }
        log.info "Sending final e-mail"
        template_final = "$baseDir/assets/final_email_template.txt"
        final_email_fields = create_final_email_fields(format.format(new Date()), summary)  
        end_mail_sent = sendFinalMail(template_final, final_email_fields, emails_map, end_mail_sent) //email_address, email_cc

        // remove work directory if pipeline is successful
        if (workflow.success) {
            if (!workflow.profile.contains('dev') && !params.sequencer.equalsIgnoreCase('AVITI')) {
                println "Pipeline terminé avec succès => suppression du workdir : $workflow.workDir"
                exec:
                    workflow.workDir.deleteDir()
            }

            if (workflow.stats.ignoredCount > 0) {
                log.warn "Warning, pipeline completed, but with errored process(es) "
                log.warn "Number of ignored errored process(es) : ${workflow.stats.ignoredCount} "
                log.warn "Number of successfully ran process(es) : ${workflow.stats.succeedCount} "
            }

            log.info "[$workflow.manifest.name] Pipeline completed successfully at $workflow.complete"
            
        } else {
            log.error "[$workflow.manifest.name] Pipeline completed with errors at $workflow.complete"
        }    
    }

    workflow.onError {
        emails_map = create_email_map()
        if (emails_map.main && emails_map.failure && !workflow.success) {
            emails_map.main = emails_map.failure
        }
        error_email_fields = create_error_email_fields(format.format(new Date())) 
        template_error = "$baseDir/assets/error_email_template.txt"
        log.info "Sending error e-mail"
        end_mail_sent = sendFinalMail(template_error, error_email_fields, emails_map, end_mail_sent)
    }
}

def getSummary() {
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
    def summary = createSummary(format.format(new Date()))
    summary['Sequencing Type'] = params.is_multiplex ? 'Multiplex' : 'Simplex'
    summary['Reference'] = params.reference_genome ?: params.reference_transcriptome?: ''
    return summary
}

// ----------------------------------
//          Auto Execution    
// ----------------------------------
// Show help message
if (params.help) {
    helpMessage(pipeline_options_map, 'S H O R T  R E A D S - N F   P I P E L I N E')
    exit 0
}

// Parameter validation
if (paramsValidation(pipeline_options_map)) { 	// true s'il manque 1 param
	log.error "\t-> Il manque au moins un paramètre obligatoire."
	exit 0
} else {
	log.info "\t-> OK"
	printOptions(pipeline_options_map)
}

// Email on start
customMailSend(
    "$baseDir/assets/begin_template.txt",
    begin_email_fields,
    emails_map,
    !workflow.resume,
    false
)

