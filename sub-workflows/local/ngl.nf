// -------------------------------------------------
// 					NGL
// -------------------------------------------------
/*
 * Tout process en lien avec NGL (Bi ou SQ)
 * 		
 * 		
*/

// -------------------------------------------------
// 					MODULES
// -------------------------------------------------
include {	BEGIN_NGLBI as BEGIN							} from "${params.shared_modules}/workflows/begin_nglbi.nf"
include {	COPY_TO_SAVE									} from "${params.shared_modules}/workflows/copy_to_save.nf"
include {	UPDATE_NGLBI_STATE_FROM_FILE as UPDATE_STATE_FQC} from "${params.shared_modules}/ngl_bi.nf"
include {	GZIP											} from "${params.shared_modules}/gzip.nf"

include {	FQ_HEADER_RENAME								} from "$baseDir/modules/local/module_core_element.nf"
include {	FILE_RENAME as RENAME_FASTQ						} from "$baseDir/modules/local/module_NGL-Bi.nf"
include {	FILE_RENAME as RENAME_INDEX						} from "$baseDir/modules/local/module_NGL-Bi.nf"
include {	NGSRG_ILLUMINA									} from "$baseDir/modules/local/module_NGL-Bi.nf"
include {	NGSRG_ELEMBIO					 				} from "$baseDir/modules/local/module_NGL-Bi.nf"
// -------------------------------------------------
// 					WORKFLOW
// -------------------------------------------------
workflow NGL {
	take:
		sequencer_name
		run_name
		bi_run_code
		sq_xp_code
		fastq
		index_files
		multiqc_html
		project
		project_hash
		is_single_cell
		demux_stat_json

	main:
		fastq_files = Channel.empty()
		md5_files = Channel.empty()

		// First steps
		BEGIN(bi_run_code, sq_xp_code,'', sequencer_name)

		nglBiRunCode = BEGIN.out.nglBiRunCode
		readsets_created = BEGIN.out.readsets_created
		ready = BEGIN.out.ready
		
		ready_for_analysis = Channel.empty()
		if(sequencer_name =~ "NovaSeq|MiSeq") {
			NGSRG_ILLUMINA(readsets_created, demux_stat_json, 'readsets')
			ready_for_analysis = NGSRG_ILLUMINA.out.ready
		} else if (sequencer_name =~ "AVITI") {
			NGSRG_ELEMBIO(readsets_created, params.inputdir, params.lane, 'readsets')
			ready_for_analysis = NGSRG_ELEMBIO.out.ready
		}

		bi_run_code = nglBiRunCode.collect().map { it.toString() }

		// Compute results
		if (is_single_cell) {
			RENAME_INDEX(index_files.map{it[1]}.collect(), readsets_created, sq_xp_code, 'fastq_index')
			fastq_files = fastq_files.mix(RENAME_INDEX.out.fastq)
		}

		RENAME_FASTQ(fastq.map{it[1]}.collect(), readsets_created, sq_xp_code, 'fastq_read')
		fastq_files = fastq_files.mix(RENAME_FASTQ.out.fastq.ifEmpty([]))
		
		if (sequencer_name =~ "AVITI") {
			FQ_HEADER_RENAME(fastq_files.flatten().map{it -> [[name: it.simpleName], it]})
			GZIP(FQ_HEADER_RENAME.out.file)
			fastq_files = GZIP.out.archive.map{it -> it[1]}
		}
		
		fq = fastq_files
			.flatMap()
			.map { it -> [[type: 'fastq', barcode:'all'], it]}
			.groupTuple()
		
		UPDATE_STATE_FQC(readsets_created, 'F-QC', multiqc_html)

		COPY_TO_SAVE(
			nglBiRunCode,
			readsets_created,
			multiqc_html.map{it -> [[type:'report'], it]},
			project,
			project_hash,
			fq,
			params.run_name,
			ready_for_analysis
		)

	emit:
		run_code = BEGIN.out.nglBiRunCode?: Channel.empty()
		readsets_file = BEGIN.out.readsets_created?: Channel.empty()
		ready = BEGIN.out.ready?: Channel.empty()
}