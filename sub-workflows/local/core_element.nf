// -------------------------------------------------
// 					CORE ELEMENT
// -------------------------------------------------
/*
 * Statistiques de démultiplexage
*/

// -------------------------------------------------
// 					MODULES
// -------------------------------------------------
include {
	DEMUX_STATS
} from "$baseDir/modules/local/module_core_element.nf"

// -------------------------------------------------
// 					LOCAL PARAMS
// -------------------------------------------------


// -------------------------------------------------
// 					WORKFLOW
// -------------------------------------------------

workflow CORE_ELEMENT {
  take:
    runManifestJson
    assigned
    unassigned
		
	main:		
		// ----------- DemultiplexStat
		DEMUX_STATS(runManifestJson, assigned, unassigned)

  emit:
		demuxStat = DEMUX_STATS.out.csv
}