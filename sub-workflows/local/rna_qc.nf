/*
	alignementSTAR
		alignementStat
		insertSizeDistribution

*/
// -------------------------------------------------
// 					RNA QC
// -------------------------------------------------
/*
 * QC des données ARN :
 * 		- Alignement contre génome de référence avec STAR
 *		- Pseudo alignement contre transcriptome de référence avec SALMON
 * 		- Rapport d'alignement avec Qualimap
*/

// -------------------------------------------------
// 					MODULES
// -------------------------------------------------
include { 	STAR_INDEX;
			STAR_ALIGN;
			SALMON_INDEX;
			SALMON_QUANT;
} from "$baseDir/modules/local/module_rna.nf"
include { GET_UNMAPPED_SALMON } from "$baseDir/modules/local/module_rna.nf"
include { 	SAMTOOLS_VIEW;
			SAMTOOLS_SORT;
			SAMTOOLS_FLAGSTATS;
} from "$baseDir/modules/local/module_dna.nf"
include { GET_UNMAPPED		} from "$baseDir/modules/local/module_core.nf"
include { DNA_QC as BWA		} from "$baseDir/sub-workflows/local/dna_qc.nf"
include { QUALIMAP 			} from "${params.shared_modules}/qualimap.nf"
include { SAMTOOLS_FAIDX	} from "${params.shared_modules}/samtools.nf"
include { SORTMERNA			} from "${params.shared_modules}/sortmerna.nf"
// -------------------------------------------------
// 					WORKFLOW
// -------------------------------------------------
workflow RNA_QC {
	take:
		fastq
		sortmerna_db

	main:
		ch_versions = Channel.empty()
		align_results = Channel.empty()

		fastq = fastq.collect{it[1]}.flatten().map { $it -> [ ($it.simpleName =~ /(.*)_R[1-2].*/)[0][1] , $it ] }.groupTuple()

		if ( "$params.reference_genome" != '' ) {
			// if indexFiles does not exist
			if ( ! file(file(params.reference_genome).getParent() + '/SAindex').exists() || params.make_star_index) {
				println "STAR index files does not exists -> Let's start genome indexing..."
				reference_genome = Channel.from(params.reference_genome)
				genome_index = SAMTOOLS_FAIDX(reference_genome).index
				star_index = STAR_INDEX(reference_genome, genome_index).index
				ch_versions = ch_versions.mix(STAR_INDEX.out.versions)
			} else {
				star_index = Channel.from(file(params.reference_genome).getParent())
			}
			STAR_ALIGN(fastq, star_index).results	// R1 et R2 en même temps
			align_results = STAR_ALIGN.out.results
			SAMTOOLS_VIEW(STAR_ALIGN.out.sam)
			SAMTOOLS_SORT(SAMTOOLS_VIEW.out.bam)
			SAMTOOLS_FLAGSTATS(SAMTOOLS_VIEW.out.bam)
			GET_UNMAPPED(SAMTOOLS_SORT.out.bam)
			qualimap_report_emitted = QUALIMAP(SAMTOOLS_SORT.out.bam).report
			ch_versions = ch_versions.mix(
				STAR_ALIGN.out.versions,
				SAMTOOLS_VIEW.out.versions,
				SAMTOOLS_SORT.out.versions,
				SAMTOOLS_FLAGSTATS.out.versions
			)

		} else if ("$params.reference_transcriptome" != '') {
			// 10X + transcriptome > use BWA
			if (params.single_cell) {
				BWA(fastq
					.collect{it[1]}
					.flatten()
					.map { $it -> [ ($it.simpleName =~ /(.*)_R[1-2]_.*/)[0][1] , $it ] }
					.groupTuple()
				)
				align_results = BWA.out.flagstats_output
				qualimap_report_emitted = BWA.out.qualimap_report
				ch_versions = ch_versions.mix(BWA.out.versions)
				
			} else {
				// if indexFiles does not exist
				if ( ! file(file(params.reference_transcriptome).getParent() + '/seq.bin').exists()) {
					println "SALMON index files does not exists -> Let's start transcriptome indexing..."
					salmon_index = SALMON_INDEX().index
					ch_versions = ch_versions.mix(SALMON_INDEX.out.versions)
				} else {
					salmon_index = Channel.from(file(params.reference_transcriptome).getParent())
				}

				def lib_type = ''
				if (params.data_nature == 'RNA-Stranded' || params.single_cell) { 
					lib_type = 'ISR'
					log.info "[RNA][SALMON] ${lib_type} library type detected."
				} else if (params.data_nature =~ 'RNA-*') { 
					lib_type = 'IU'
					log.info "[RNA][SALMON] ${lib_type} library type detected."
				} else { 
					log.warning "[RNA][SALMON] Unknown library type ! No transcript quantification can be performed."
				}
				ch_lib_type = Channel.value(lib_type)

				align_results = SALMON_QUANT(
					fastq, 
					salmon_index, 
					ch_lib_type
				).results
				GET_UNMAPPED_SALMON(fastq, align_results)
				qualimap_report_emitted= Channel.empty()
				ch_versions = ch_versions.mix(SALMON_QUANT.out.versions)
			}

		} else {
			// If Qualimap and Samtools were not executed
			System.out.println "Pas de référence genomique ou transcriptomique renseignée, on ne peut pas faire d'alignement"
			qualimap_report_emitted =  Channel.empty()
			flagstats_output_emitted = Channel.empty()
		}

		SORTMERNA(fastq, sortmerna_db.collect())

	emit:
		align_results = align_results
		sortmerna_log = SORTMERNA.out.log
		qualimap_report = qualimap_report_emitted
		//flagstats_output = flagstats_output_emitted
		versions = ch_versions
}