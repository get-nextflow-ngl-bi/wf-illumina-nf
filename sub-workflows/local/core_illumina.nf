// -------------------------------------------------
// 					CORE ILLUMINA
// -------------------------------------------------
/*
 * Statistiques de démultiplexage
 * IlluminaFilter
*/

// -------------------------------------------------
// 					MODULES
// -------------------------------------------------
include {
	PREP_DEMUXSTAT;
	DEMUX_STATS;
	ILLUMINA_FILTER;
} from "$baseDir/modules/local/module_core.nf"


// -------------------------------------------------
// 					LOCAL PARAMS
// -------------------------------------------------


// -------------------------------------------------
// 					WORKFLOW
// -------------------------------------------------

workflow CORE_ILLUMINA {
   take:
		sampleSheet
		demuxStatXML
		demuxSummary
		fastq
		
	main:		
		ch_versions = Channel.empty()

		// ----------- DemultiplexStat
		PREP_DEMUXSTAT(sampleSheet)
		DEMUX_STATS(demuxStatXML, PREP_DEMUXSTAT.out, demuxSummary)
		
		// ----------- Illumina Filter  // ou SubsetSeqFiles : dans quel cas on fait l'un ou l'autre ????
		if ("$params.sequencer" =~ /NovaSeq.*/ && params.is_multiplex) {
			System.out.println "Les données ne nécessite pas de passer par IlluminaFilter"
			fastq_good = fastq
		} else {	// Si MiSeq ou Nova + noIndex
			ILLUMINA_FILTER(fastq)
			fastq_good = ILLUMINA_FILTER.out.reads
			ch_versions = ch_versions.mix(ILLUMINA_FILTER.out.versions)
		}

    emit:
        fastq = fastq_good
		demuxStat = DEMUX_STATS.out.demultiplexStatsTSV
		versions = ch_versions
}

