// -------------------------------------------------
// 					DNA QC
// -------------------------------------------------
/*
 * QC des données ADN :
 * 		- Alignement contre génome de référence
 * 		- Rapport d'alignement avec Qualimap
*/

// -------------------------------------------------
// 					MODULES
// -------------------------------------------------
include { 	BWA_ALIGNMENT 		} from "$baseDir/modules/local/module_dna.nf"
include {	SAMTOOLS_VIEW 		} from "$baseDir/modules/local/module_dna.nf"
include {	SAMTOOLS_SORT 		} from "$baseDir/modules/local/module_dna.nf"
include {	SAMTOOLS_FLAGSTATS	} from "$baseDir/modules/local/module_dna.nf"
include { 	GET_UNMAPPED		} from "$baseDir/modules/local/module_core.nf"
include { 	QUALIMAP 			} from "${params.shared_modules}/qualimap.nf"

// -------------------------------------------------
// 					WORKFLOW
// -------------------------------------------------
workflow DNA_QC {
	take:
		fastq

	main:
		ch_versions = Channel.empty()

		if ( "$params.reference_genome" != '' || "$params.reference_transcriptome" != '') {
			BWA_ALIGNMENT(fastq)
			SAMTOOLS_VIEW(BWA_ALIGNMENT.out.sam)
			SAMTOOLS_SORT(SAMTOOLS_VIEW.out.bam)
			SAMTOOLS_FLAGSTATS(SAMTOOLS_VIEW.out.bam)
			QUALIMAP(SAMTOOLS_SORT.out.bam)
			GET_UNMAPPED(SAMTOOLS_SORT.out.bam)

			qualimap_report_emitted =  QUALIMAP.out.report
			flagstats_output_emitted = SAMTOOLS_FLAGSTATS.out.txt
			bam_output_emitted = SAMTOOLS_SORT.out.bam
			ch_versions = ch_versions.mix(
				BWA_ALIGNMENT.out.versions,
				SAMTOOLS_VIEW.out.versions,
				SAMTOOLS_SORT.out.versions,
				SAMTOOLS_FLAGSTATS.out.versions
			)

		} else {
			System.out.println "Pas de référence genomique ou transcriptomique renseignée, on ne peut pas faire d'alignement"
			// If Qualimap and Samtools were not executed
			qualimap_report_emitted =  Channel.empty()
			flagstats_output_emitted = Channel.empty()
			bam_output_emitted = Channel.empty()
		}

	emit:
		qualimap_report = qualimap_report_emitted
		flagstats_output = flagstats_output_emitted
		bam = bam_output_emitted
		versions = ch_versions
}