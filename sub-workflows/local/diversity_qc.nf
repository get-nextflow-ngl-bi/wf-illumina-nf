
/*
	pairedEnd merging (FLASH)
		if analyse 16S AND banque fournie, alors :
			Assignation on a subset of sequences
*/

// -------------------------------------------------
// 					MODULES
// -------------------------------------------------
include {	JOIN_PAIR;
			BLAST_N				} from "$baseDir/modules/local/module_diversity.nf"
include {	GUNZIP				} from "${params.shared_modules}/gzip.nf"
include {	SEQTK_SAMPLE as SUBSET;
			SEQTK_SEQ_A as FQ_TO_FA	} from "${params.shared_modules}/seqtk.nf"
include {	KRONA_BLAST		 		} from "${params.shared_modules}/krona.nf"


// -------------------------------------------------
// 					WORKFLOW
// -------------------------------------------------
workflow DIVERSITY_QC {
	take:
		fastq

	main:
		ch_versions = Channel.empty()

		// Pairs merging
		JOIN_PAIR(fastq)
		ch_versions = ch_versions.mix(JOIN_PAIR.out.versions)

		// SubsetAssignation
		if (params.assignation_databank != '') {
			GUNZIP(JOIN_PAIR.out.extendedFrags)
			SUBSET(GUNZIP.out)

			// -- Fastq to Fasta
			FQ_TO_FA(SUBSET.out)

			// -- Taxonomic assignation
			BLAST_N(FQ_TO_FA.out.fasta, params.assignation_databank)
			ch_versions = ch_versions.mix(BLAST_N.out.versions)
			KRONA_BLAST(BLAST_N.out.results)
			krona_html = KRONA_BLAST.out.html

		} else {
			krona_html = Channel.empty()
		}

	emit:
		extendedFrags = JOIN_PAIR.out.extendedFrags
		histogram = JOIN_PAIR.out.histogram
		logs = JOIN_PAIR.out.logs
		krona = krona_html
		versions = ch_versions
}