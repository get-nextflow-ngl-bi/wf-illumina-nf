
// -------------------------------------------------
// 					CORE PIPELINE
// -------------------------------------------------
/*
 * QC des reads
 * Recherche contaminations
 * Recherche duplicats
*/

// -------------------------------------------------
// 					MODULES
// -------------------------------------------------
include {
	FASTQC;
	FASTQSCREEN;
	DUPLICATED_READS;
} from "$baseDir/modules/local/module_core.nf"
include { GUNZIP			} from "${params.shared_modules}/gzip.nf"
include { SEQTK_SAMPLE 		} from "${params.shared_modules}/seqtk.nf"
//-------------------------------------------------

inNGL=true
forceNewReadset=false
isResume=workflow.resume

//-------------------------------------------------
workflow CORE {
	take:
		ch_read
		
	main:
		ch_versions = Channel.empty()

		// ----------- FASTQC
		FASTQC(ch_read)
		ch_versions = ch_versions.mix(FASTQC.out.versions)
		
		// ----------- ContaminationSearch
		FASTQSCREEN(ch_read)
		ch_versions = ch_versions.mix(FASTQSCREEN.out.versions)

		// ----------- Recherche Duplicats
		GUNZIP(ch_read)
		
		// ----------- Sous-échantillonnage
		if (params.no_subset) {
			unzipped_fastq = GUNZIP.out
		} else {
			/*
			if ( "$params.coverage" != 0 && ("$params.reference_genome" != '' || "$params.reference_transcriptome" != '')) {
				reference = params.reference_genome ?: params.reference_transcriptome
				GET_NB_SEQ_FROM_COV(reference, params.coverage, ch_read[0])
				nb_seq = GET_NB_SEQ.out.value
			} else {
				nb_seq = params.subset_seq
			}
			*/
			SEQTK_SAMPLE(GUNZIP.out/*, nb_seq*/)
			unzipped_fastq = SEQTK_SAMPLE.out
		}

		DUPLICATED_READS(unzipped_fastq
			.collect{it[1]}
			.flatten()
			.map { $it -> [ ($it.simpleName =~ /(.*)_R[1-2].*/)[0][1] , $it ] }
			.groupTuple()
		) // need fastq paired !!!
		ch_versions = ch_versions.mix(DUPLICATED_READS.out.versions)
		
	emit:
		fastqc_report = FASTQC.out.zip ?: Channel.empty()
		fastqscreen_report = FASTQSCREEN.out.report ?: Channel.empty()
		fastp_report = DUPLICATED_READS.out.json
		subset_fastq = unzipped_fastq
		versions = ch_versions
}
