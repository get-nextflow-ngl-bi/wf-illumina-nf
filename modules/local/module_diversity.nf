/*
 *	Module pour les analyses des données 16S et Amplicon
*/

process JOIN_PAIR {
    tag "$sample"

	input:
		tuple val(sample), path(reads)
	
	output:
		tuple val(sample), path("*.extendedFrags.fastq.gz"), emit: extendedFrags
        tuple val(sample), path("*.notCombined_*.fastq.gz"), emit: notCombined
        tuple val(sample), path("*.log"), emit: logs
        tuple val(sample), path("*.hist"), emit: histogram
        path("versions.yml"), emit: versions
	
	script:
    def args = task.ext.args ?: ''
    def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		flash \\
            $reads \\
            -z \\
            -t ${task.cpus} \\
            -o ${sample} \\
            $args \\
            > ${sample}_flash.log

        mv ${sample}.hist ${sample}_flash.hist

cat <<-END_VERSIONS > versions.yml
${analyse_type} - flash: \$( flash --version | sed \'/^FLASH v/!d; s/.*v//' )
END_VERSIONS
	"""
}

// Blastn
process BLAST_N {
    tag "$sample"

    input:
        tuple val(sample), path(fasta)
        val db

    output:
        tuple val(sample), path("*.blastn"), emit: results
        path("versions.yml"), emit: versions

    script:
    def args = task.ext.args ?: ''
    def analyse_type = task.ext.analyse_type ?: params.default_label
    """
        db_dir=\$(dirname $db)
        [[ `find -L \$db_dir -name "*.00.idx"` ]] && isIndexed='true' || isIndexed='false'

        blastn \\
            -num_threads $task.cpus \\
            -db $db \\
            -query $fasta \\
            -use_index \$isIndexed \\
            $args \\
            -out ${sample}.blastn 

    cat <<-END_VERSIONS > versions.yml
    ${analyse_type} - blastn: \$(blastn -version 2>&1 | sed '/^.*blastn: /!d;  s/.*: //')
    END_VERSIONS
    """

}
