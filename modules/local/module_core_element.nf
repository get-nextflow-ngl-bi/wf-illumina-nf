/*
 *	Module pour les analyses de base des données Element Biosciences
*/

process DEMUX_STATS {
	label 'demux'
    label 'little_R'

	input:
		path runManifestJson
        path assigned
        path unassigned
	
	output:
		path "demultiplexStat_*.csv", emit: csv
	
	script:
    def threshold = task.ext.threshold ?: ''
    def lane = params.lane ?: '1'
	"""
    demuxStatsElement.R \\
        --assigned $assigned \\
        --unassigned $unassigned \\
        --runManifest $runManifestJson \\
        --lane $lane \\
        $threshold
	"""
}

process FQ_HEADER_RENAME {
	tag "$meta.name"

	input:
		tuple val(meta), path(fastx)

	output:
		tuple val(meta), path("${fileName}.${extension}"), emit: file

	script:
	def args = task.ext.args ?: ''
    extension = "fastq"
    if ("$fastx" ==~ /.+\.fasta|.+\.fasta.gz|.+\.fa|.+\.fa.gz|.+\.fas|.+\.fas.gz|.+\.fna|.+\.fna.gz/) {
        extension = "fasta"
    }
    fileName = fastx.toString() - ".${extension}" - '.gz'	// remove also .gz if exists
	if (fastx.toString().endsWith('.gz')) {
	"""
		pigz $args -dc -p ${task.cpus} ${fastx} | \
		awk -F':' 'BEGIN{OFS=":"} {if (\$0 ~ /^@/) {\$2=""; sub("::", ":", \$0)} print}' - > ${fileName}.${extension}
	"""
	} else {
	"""
		awk -F':' 'BEGIN{OFS=":"} {if (\$0 ~ /^@/) {\$2=""; sub("::", ":", \$0)} print}' ${fastx} > ${fileName}.${extension}
    """
	}
}