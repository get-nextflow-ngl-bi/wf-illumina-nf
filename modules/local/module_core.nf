/*
 *	Module pour les analyses de base du pipeline
*/

process PREP_DEMUXSTAT {
	label 'demux'

	input:
		path SampleSheet
	
	output:
		path "indexNumber.tsv"
	
	script:
	"""
		extractInfoForDemuxStats.pl --sampleSheet $SampleSheet
	
	"""
}

process DEMUX_STATS {
	label 'demux'
	
	input:
		path DemuxStatXML
		path IndexNumberFile
		path DemuxSummary
	
	output:
		path 'demultiplexStats.log', emit: log
		path "DemultiplexStats.tsv", emit: demultiplexStatsTSV
	
	script:
	"""
		demuxStatsFromXML.R --xml $DemuxStatXML --indexNumber $IndexNumberFile --demuxSum $DemuxSummary > demultiplexStats.log
	"""
}

process FASTQC {
	
	tag " $name"
	
	input:
		tuple val(name), path(read)
		
	output:
		tuple val(name), path("*_fastqc.html") , emit: html
		tuple val(name), path("*_fastqc.zip") , emit: zip
		path("versions.yml") , emit: versions
		// path log files
		
	script:
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		fastqc -t $task.cpus --nogroup --noextract --outdir ./ ${read}

	cat <<-END_VERSIONS > versions.yml
	${analyse_type} - fastqc: \$( fastqc --version | sed '/FastQC v/!d; s/.*v//' )
	END_VERSIONS
	"""
}


process ILLUMINA_FILTER {
	tag " $name"
	
	input:
		tuple val(name), path(read)
	
	output:
		tuple val("$name"), path("*.fastq.gz"), emit: reads
		path("*.output"), emit: log
		path("versions.yml") , emit: versions
	
	script:
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		zcat $read | fastq_illumina_filter --keep N -v 2> ${name}.output | gzip -c -f > ${name}_filtered.fastq.gz	
		
	cat <<-END_VERSIONS > versions.yml
	'${analyse_type} - fastq_illumina_filter': \$( fastq_illumina_filter -h | head -1 | sed -n 's/.*version \\([0-9.]*\\).*/\\1/p'  )
	END_VERSIONS
	"""
			//	
}

process FASTQSCREEN {	
	tag " $sample"
	
	input:
		tuple val(sample), path(reads)
	
	output:
		tuple val(sample), path("*.txt"), emit: report
		path("versions.yml") , emit: versions
	
	script:
	def args = task.ext.args ?: ''
	def defaultConf = "${baseDir}/assets/fastq_screen.conf_example"
	def inputConf = "${params.inputdir}/fastq_screen.conf"
	def confFile = file(inputConf).exists() ? inputConf : defaultConf
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		fastq_screen \\
			$reads \\
			--conf ${confFile} \\
			$args

	cat <<-END_VERSIONS > versions.yml
	${analyse_type} - fastq_screen: \$( fastq_screen --version | sed '/FastQ Screen v/!d; s/.*v//' )
	END_VERSIONS
	"""
}

process DUPLICATED_READS {
	tag "$sample"

	input:
		tuple val(sample), path(fastq)

	output:
		tuple val(sample), path("*.json"), emit: json
		tuple val(sample), path("*.log"), emit: log
		path("versions.yml") , emit: versions

	shell:
	R1_name=file(fastq[0]).simpleName
	R2_name=file(fastq[1]).simpleName
	args = task.ext.args ?: ''
	analyse_type = task.ext.analyse_type ?: params.default_label
	'''
		fastp \
		-i !{fastq[0]} \
		-o !{R1_name}_dedupl.fastq \
		-I !{fastq[1]} \
		-O !{R2_name}_dedupl.fastq \
		--disable_adapter_trimming \
		--disable_quality_filtering \
		--disable_length_filtering \
		--json !{R1_name}_fastp.json \
		!{args} \
		2> !{R1_name}.log

	cat <<-END_VERSIONS > versions.yml
	!{analyse_type} - fastp: $(fastp --version 2>&1 | sed -e 's/fastp //g')
	END_VERSIONS
	'''
}

process GET_UNMAPPED {
	tag "$sample"
	
	label 'samtools'

	input:
		tuple val(sample), path(sorted_bam)	// [[id], [files, ...]]

	output:
		tuple val(sample), path("*.log"), emit: log
		tuple val(sample), path("*.fastq.gz"), emit: fastq_gz
		path("versions.yml"), emit: versions

	script:
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		samtools fastq -f 4 ${sorted_bam} 2>> ${sample}_unmapped.log | gzip -c > ${sample}_unmapped.fastq.gz
	
	cat <<-END_VERSIONS > versions.yml
	${analyse_type} - samtools: \$(samtools --version 2>&1 | sed '/^.*samtools/!d; s/.* //')
	END_VERSIONS
	"""
}

process MERGE_LANES {	
	tag "$sample"
	
	input:
		tuple val(sample), path(reads)
	
	output:
		tuple val(sample), path("*_R{1,2}_001.fastq.gz"), emit: fastq
	
	script:
	def args = task.ext.args ?: ''
	"""
	#!/bin/bash

	cat `ls *R1*` > ${sample}_R1_001.fastq.gz
	cat `ls *R2*` > ${sample}_R2_001.fastq.gz

	"""
}

/* --------------------------------------------------------------------
 * 								OLD PROCESS
 * --------------------------------------------------------------------
*/
process decoupageSS {
	// Not used anymore
	publishDir path: "${params.outdir}/SampleSheets" , mode: 'copy'
	
	input:
		path multiSS
		
	output:
		path '*'
		
	shell:
	"""
		extractReads.pl $multiSS NovaSeq
	
	"""
}

process maskMaker {
	publishDir path: "${params.outdir}/Demux" , mode: 'copy'
	
	input:
		path SampleSheet
		path RunInfoXML
	
	output:
		path 'Run.conf'
	
	script:
	"""
		extractInfo.pl -s $SampleSheet -r $RunInfoXML
	
	"""
}

process bcl2fastq {
	publishDir path: "${params.outdir}/Demux/Reads" , mode: 'copy'
	
	echo=true
	
	input:
		path SampleSheet
		path Runconf
		val mismatchNumber
		path rawdata_location
		
	//output:
		//path "*"
		
	shell:
	"""
		mask=\$(grep 'MASQUE' !{Runconf} | cut -d'=' -f2)
		echo "bcl2fastq -p 10 -r 4 -w 4 \${mask} --barcode-mismatches !{mismatchNumber} --output-dir ./ -R !{rawdata_location} --sample-sheet !{SampleSheet} -l DEBUG"
		
	"""
}

process search_conta_bwa {
	// aln command uses ~3.2GB memory and the sampe command uses ~5.4GB
	publishDir path: "${params.outdir}/ContaminationSearch/tmp" , mode: 'copy'
	module 'bioinfo/bwa-0.7.17'
	time { 20.m * task.attempt }
	memory { 5.GB * task.attempt }
	
	input:
		tuple val(name), path(read)
		each genomeRef
		
	output:
		tuple val("${name}_${genomeName}"), path("${name}_${genomeName}.sam"), emit: sam
		
	script:
	genomeName=file(genomeRef).simpleName
	"""
		bwa aln $genomeRef $read 2>> ${name}_${genomeName}.err | bwa samse $genomeRef - $read > ${name}_${genomeName}.sam 2>> ${name}_${genomeName}.err
	"""
}

process search_conta_samtools {
	publishDir path: "${params.outdir}/ContaminationSearch" , mode: 'copy'
	
	module 'bioinfo/samtools-1.9'
	time { 10.m * task.attempt }
	
	tag " $sample"
	
	input:
		tuple val(name), path("*")
	
	output:
		//tuple val("$name"), path("*")
		path("*.txt")
	
	script:
	"""
		samtools view -SF 260 ${name}.sam 2>> ${name}.err | cut -f1 - 2>> ${name}.err | sort - > ${name}.txt 2>> ${name}.err
	"""
}

process search_conta_summary {
	publishDir path: "${params.outdir}/ContaminationSearch" , mode: 'copy'
	
	time { 10.m * task.attempt }
	memory '1.GB'
	
	tag " $sample"
	
	input:
		//tuple val(name), path("*")
		path("*")
		
	output:
		path("*.yaml")
		
	script:
	"""
		contaCounter.pl ./
	"""
}