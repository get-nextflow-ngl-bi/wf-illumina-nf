/*
 * Ensemble de process pour l'interraction avec NGL-Bi
 * Process pour la création de traitement SAV
 */


process prepareReadSetCreation {
	publishDir path: "${params.outdir}/NGLBi" , mode: 'copy'
	
	input:
		path sampleSheet
		path runNGLBiCreated
		
	output:
		file 'readSetCreation.info'
		
	script:
	"""
		extractInfoForReadSets.pl --sampleSheet $sampleSheet --runNGLBi $runNGLBiCreated
	"""
}

process TREATMENT_DEMUXSTAT {
	label 'ngl'

	input:
		val nglCode
		path csvFile
		val lane

	output:
		path("*.log")
		val 1, emit: ready

	script:
	def args = task.ext.args ?: ''
	forceOption = workflow.resume ? "--force" : ''
	def level = lane ? "run_${lane}" : 'readsets'
	"""
		perl ${params.ngl_bi_client}/GeT/perl/illumina/createNGL-BiTreatmentDemultiplexStat.pl \\
			--code $nglCode \\
			--stat $csvFile \\
			${forceOption} \\
			$args \\
			1> treatment_demux_${level}.log
	"""
}

process FILE_RENAME {
	label 'ngl'

	input:
		path(inputDirectory)
		path(readsetsFile)
		val(sqXpCode)
		val(fileType)

	output:
		path("outputs/*.fastq.gz"), emit: fastq
		path("*.log"), emit: log
		val 1, emit: ready

	script:
	def args = task.ext.args ?: ''
	"""
		perl ${params.ngl_bi_client}/GeT/perl/illumina/file_rename.pl \\
			--input ./ \\
			--readsets $readsetsFile \\
			--sqExperimentCode $sqXpCode \\
			$args \\
			1> ${fileType}_rename.log
	"""
}

process NGSRG_ILLUMINA {
	label 'ngl'

	input:
		path nglFile
		path file
		val level

	output:
		path("*.log"), emit: log
		val 1, emit: ready

	script:
	def args = task.ext.args ?: ''
	def statFileOpt = file.endsWith('Stats.json') ? "--demuxStatFile $file" : ''
	"""
		perl ${params.ngl_bi_client}/GeT/perl/illumina/createNGL-BiTreatmentNGSRG.pl \\
			--objectFile $nglFile \\
			--level $level \\
			$statFileOpt \\
			1> treatment_ngsrg_${level}.log
	
	"""
}

process NGSRG_ELEMBIO {
	label 'ngl'

	input:
		path nglFile
		path demuxDirectory
		val lane
		val level

	output:
		path("*.log"), emit: log
		val 1, emit: ready

	script:
	def args = task.ext.args ?: ''
	"""
		perl ${params.ngl_bi_client}/GeT/perl/elemBio/createNGL-BiTreatmentNGSRG.pl \\
			--objectFile $nglFile \\
			--pathdemuxRunStatsFile $demuxDirectory/RunStats.json \\
			--pathdemuxRunParametersFile $demuxDirectory/RunParameters.json \\
			--laneNumberToWorkOn $lane \\
			--level $level \\
			$args \\
			1> treatment_ngsrg_${level}_${lane}.log
	"""
}