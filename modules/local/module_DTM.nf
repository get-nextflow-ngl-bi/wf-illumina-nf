/*
 *	Module pour la gestion des analyses particulières dans le cadre d'un DTM
*/

process PARSE_REPORTS {	
	tag "$sample"
	
	input:
		tuple val(sample), path(fastp_json_report)
		tuple val(sample), path(qualimap_folder)
		tuple val(sample), path(gc_bias_report)
		
	output:
		tuple val(sample), path("*.csv"), emit: csv
		
	script:
	"""
		bash parse_reports.sh $sample $fastp_json_report $qualimap_folder $gc_bias_report
	"""
}

