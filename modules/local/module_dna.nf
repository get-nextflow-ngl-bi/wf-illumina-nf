/*
 *	Module pour l'alignement des reads ADN sur génome de référence et des statistiques associées
*/

process BWA_ALIGNMENT {
	tag "$sample"

	label 'bwa'
	
	input:
		tuple val(sample), path(reads)
		
	output:
		tuple val(sample), path("*.log"), emit: log
		tuple val(sample), path("*.sam"), emit: sam
		path("versions.yml"), emit: versions
		
	script:
	def reference = params.reference_genome ?: params.reference_transcriptome
	def referenceName=file(reference).toString().split('/')[6]
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		bwa mem ${reference} ${reads} -t ${task.cpus} 1> ${sample}_${referenceName}.sam 2> ${sample}_${referenceName}.log
	
	cat <<-END_VERSIONS > versions.yml
	${analyse_type} - bwa: \$( bwa 2>&1 | sed '/^Version/!d; s/.*: //' )
	END_VERSIONS
	"""
}

process SAMTOOLS_VIEW { 
	tag "$sample"
	
	label 'samtools'
	
	input:
		tuple val(sample), path(sam)
		
	output:
		tuple val(sample), path("*.bam"), emit: bam
		path("versions.yml"), emit: versions
		
	script:
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		samtools view -bS ${sam} -@ ${task.cpus} > ${sample}.bam
	
	cat <<-END_VERSIONS > versions.yml
	${analyse_type} - samtools: \$(samtools --version 2>&1 | sed '/^.*samtools/!d; s/.* //')
	END_VERSIONS
	"""
}

process SAMTOOLS_SORT {
	tag "$sample"
	
	label 'samtools'

	input:
		tuple val(sample), path(bam)

	output:
		tuple val(sample), path("*.log"), emit: log
		tuple val(sample), path("*.bam"), emit: bam
		path("versions.yml"), emit: versions

	script:	
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""	
		samtools sort ${bam} -o ${sample}_sorted.bam 2>> ${sample}.log
	
	cat <<-END_VERSIONS > versions.yml
	${analyse_type} - samtools: \$(samtools --version 2>&1 | sed '/^.*samtools/!d; s/.* //')
	END_VERSIONS
	"""
}

process SAMTOOLS_FLAGSTATS {
	tag "$sample"
	
	label 'samtools'
	label 'alignmentStats'

	input:
		tuple val(sample), path(bam)

	output:
		tuple val(sample), path("*.log"), emit: log
		tuple val(sample), path("*.txt"), emit: txt
		path("versions.yml"), emit: versions

	script:
	def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		samtools flagstat ${bam} > ${sample}_flagstat.txt 2>> ${sample}.log
	
	cat <<-END_VERSIONS > versions.yml
	${analyse_type} - samtools: \$(samtools --version 2>&1 | sed '/^.*samtools/!d; s/.* //')
	END_VERSIONS
	"""
}




/*
process alignmentQualityStats {
	publishDir path: "${params.outdir}/alignmentStats/cigar" , mode: 'copy'
	
	label 'cigar'

	input:
		tuple val(sample), path(bam)

	output:
		tuple val(sample), path("*.log"), emit: log
		tuple val(sample), path("*.csv"), emit: csv
		tuple val(sample), path("*.png"), emit: graph

	script:
	cigarOptions = params.split_reads ? "--readsplit" : ""
	
	if (params.pairedEnd) {
		"""
			python
			samtools view -F0x0100 ${bam} | cigarlineGraph.py -i - -t ${sample}_R1.csv ${sample}_R2.csv -o ${sample}_R1.png ${sample}_R2.png ${cigarOptions} 2> ${sample}.log
		"""
	} else {
		"""
			samtools view -F0x0100 ${bam} | cigarlineGraph.py -i - -t ${sample}_R1.csv ${cigarOptions} 2> ${sample}.log
		"""
	}
}

process alignmentSummary {
	publishDir path: "${params.outdir}/alignmentStats/summary" , mode: 'copy'

	label 'samtools'

	input:
		tuple val(sample), path(bam)

	output:
		tuple val(sample), path("*.stat"), emit: stat

	script:
	"""
		samtools view -F0x0100 -bh ${bam} | samtools flagstat - > ${sample}.stat
	"""
}

process readAlignementSummary  {	// addTreatment
	publishDir path: "${params.outdir}/alignmentStats/summary" , mode: 'copy'

	input:
		tuple val(sample), path(statFile)

	output:
		tuple val(sample), path("*.log"), emit: log

	script:
	"""
		alignementStatTreatment.pl --file ${statFile} 1> ${sample}.log
	"""


}

		//alignmentQualityStats(samtoolsSort.out.bam)
		//alignmentSummary(samtoolsSort.out.bam)
		//readAlignementSummary(alignmentSummary.out.stat)
		

*/