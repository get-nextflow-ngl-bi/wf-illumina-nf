process bar {
	publishDir path: "/home/sbsuser/work/Nextflow/wf-illumina-nf/results" , mode: 'copy'

	input:
		path x
		path y

	output:
		path 'bar.txt', emit: fichier_de_sortie
		// path 'foo.txt', emit: other_file

	script:
	"""
		(cat $x; head $y ) > bar.txt
    """
}

