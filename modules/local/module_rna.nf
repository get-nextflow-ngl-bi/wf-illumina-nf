/*
 *	Module pour l'alignement des reads ARN sur génome/trasncriptome de référence et extraction des statistiques associées
*/

process SALMON_INDEX {
    tag "$params.project"
    
    output:
        path("index/"), emit: index
        path("versions.yml"), emit: versions

    script:
    def analyse_type = task.ext.analyse_type ?: params.default_label
    """
        salmon index \
        -t ${params.reference_transcriptome} \
        -i ./index \
        --threads ${task.cpus}

    cat <<-END_VERSIONS > versions.yml
    ${analyse_type} - salmon: \$(salmon --version | sed 's/salmon //')
    END_VERSIONS
    """
}


process SALMON_QUANT {
    tag "$sample"

    input:
        tuple val(sample), path(reads)
        path(index)
        val(lib_type)

    output:
        tuple val(sample), path("$sample/"), emit: results
        path("versions.yml"), emit: versions

    script:
    def args = task.ext.args ?: ''
    def R1 = reads.find { it =~ /.*_R1_.*/}
    def R2 = reads.find { it =~ /.*_R2_.*/}
    def analyse_type = task.ext.analyse_type ?: params.default_label
    """
        salmon quant \\
            --libType ${lib_type} \\
            --validateMappings \\
            -o ./${sample}/ \\
            --index ${index} \\
            -1 ${R1} \\
            -2 ${R2} \\
            $args \\
            2> /dev/null

    cat <<-END_VERSIONS > versions.yml
    ${analyse_type} - salmon: \$(echo \$(salmon --version) | sed -e "s/salmon //g")
    END_VERSIONS
    """
}

process GET_UNMAPPED_SALMON {
	tag "$sample"
	
	label 'seqkit'

	input:
		tuple val(sample), path(reads)	// [[id], [R1, R2]]
        tuple val(sample), path(salmon_results)

	output:
		tuple val(sample), path("*.fastq.gz"), emit: fastq_gz, optional: true
		path("versions.yml"), emit: versions

	script:
    def R1 = reads.find { it =~ /.*_R1_.*/}
    def R2 = reads.find { it =~ /.*_R2_.*/}
    def analyse_type = task.ext.analyse_type ?: params.default_label
	"""
		grep 'u\$' ${salmon_results}/aux_info/unmapped_names.txt | cut -d' ' -f1 > unmapped_id

        seqkit grep -f unmapped_id ${R1} -o unmapped_R1.fastq
        seqkit grep -f unmapped_id ${R2} -o unmapped_R2.fastq

        cat unmapped_R1.fastq unmapped_R2.fastq | gzip -c > ${sample}_unmapped.fastq.gz
        
	cat <<-END_VERSIONS > versions.yml
    "${task.process}":
    ${analyse_type} - seqkit: \$( seqkit version | sed 's/seqkit v//' )
    END_VERSIONS
	"""
}

process STAR_INDEX {
    tag "$params.project"
    label "star"

    input:
        path(fasta)
        path(fai)
    
    output:
        path("index/"), emit: index
        path("versions.yml"), emit: versions

    script:
    // renamme en .fa ?? utile ??
    def args = task.ext.args ?: ''
    def memory = task.memory ? "--limitGenomeGenerateRAM ${task.memory.toBytes() - 100000000}" : ''
    def analyse_type = task.ext.analyse_type ?: params.default_label
    """
        NUM_BASES=`gawk '{sum = sum + \$2}END{if ((log(sum)/log(2))/2 - 1 > 14) {printf "%.0f", 14} else {printf "%.0f", (log(sum)/log(2))/2 - 1}}' ${fai}`
        
        mkdir index
        STAR \\
            --runMode genomeGenerate \\
            --genomeDir index/ \\
            --genomeFastaFiles $fasta \\
            --runThreadN $task.cpus \\
            --genomeSAindexNbases \$NUM_BASES \\
            $args

    cat <<-END_VERSIONS > versions.yml
    ${analyse_type} - star: \$(STAR --version)
    END_VERSIONS
    """
}

process STAR_ALIGN {
    tag "$sample"
    label "star"

    input:
        tuple val(sample), path(reads)
        each index

    output:
        tuple val(sample), path("${sample}_Log.final.out"), emit: results
        tuple val(sample), path("${sample}_Log.out"), emit: log
        tuple val(sample), path("${sample}_Aligned.out.sam"), emit: sam
        path("versions.yml"), emit: versions

    script:
    def args = task.ext.args ?: ''
    def read_files_cmd = reads[0].endsWith('.gz') ? '--readFilesCommand zcat' : ''
    def analyse_type = task.ext.analyse_type ?: params.default_label
    """
        STAR \\
            --outFileNamePrefix  ${sample}_ \\
            --genomeDir $index/ \\
            --runThreadN $task.cpus \\
            --outSAMunmapped Within \\
            --readFilesIn $reads \\
            $read_files_cmd

    cat <<-END_VERSIONS > versions.yml
    ${analyse_type} - star: \$(STAR --version)
    END_VERSIONS
    """
}